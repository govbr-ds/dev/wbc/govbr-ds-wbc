<!-- markdownlint-disable MD041 -->

## Componentes

### Conformidade Visual e Comportamental

- [ ] Comportamento está igual ao GovBR-DS.
- [ ] Atende às especificações visuais, comportamentais e anatomia de Design.
- [ ] Componente está [flexível](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/desenvolvimento/web-components/stencil/flexibilidade/)? Podemos flexibilizar outras partes?

### Código

- [ ] Propriedades foram testadas e documentadas.
- [ ] Eventos foram testados e documentados.
- [ ] Os eventos são disparados em **todas** as interações do usuário com o componente? Se não, por que?
- [ ] Métodos foram testados e documentados.
- [ ] Slots foram testados e documentados.
- [ ] Parts foram testados e documentados.
- [ ] Descrição do componente segue o padrão de documentação dos componentes.
- [ ] Componente precisa ser associado a um `<form>`? Se sim, o `formAssociated` foi corretamente configurado com `props` e `events`?
- [ ] Os atributos obrigatórios foram definidos como `required`?
- [ ] Os valores padrão foram definidos corretamente?
- [ ] As descrições das propriedades, eventos, métodos, slots e estão claras, objetivas e seguem o padrão de documentação?

### Qualidade

- [ ] Código construído seguindo os [princípios do Clean Code](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/desenvolvimento/web-components/stencil/principios-clean-code/).
- [ ] Nomenclaturas estão conforme o [padrão do projeto](https://govbr-ds.gitlab.io/tools/govbr-ds-wiki/desenvolvimento/web-components/stencil/padrao-nomenclatura/).
- [ ] Código contém comentários explicativos para partes complexas.
- [ ] Comentários desnecessários foram removidos (evitar deixar código comentado "para não perder").
- [ ] Sem `console.log` ou comandos de `debug` no código.

### Testes

#### Unitários (`*.spec.tsx`)

- [ ] Estado inicial sem props foi testado e está correto
- [ ] Props foram testadas com:
  - [ ] Valores válidos
  - [ ] Valores inválidos/limite
  - [ ] Alterações durante execução
- [ ] Classes CSS condicionais foram testadas
- [ ] Renderização de slots foi validada
- [ ] Lógica interna (métodos/cálculos) foi testada
- [ ] Validações básicas (required, min/max) foram testadas

#### E2E (`*.e2e.ts`)

- [ ] Eventos foram testados (click, blur, change etc.)
- [ ] Interações do usuário foram simuladas
- [ ] Integrações com DOM foram validadas
- [ ] Estilização visual foi verificada
- [ ] Shadow DOM (slots/parts) foi testado
- [ ] Testes em navegador real foram executados

### Documentação

- [ ] Documentação automática (`{component}/readme.md`) foi gerada/atualizada.
- [ ] Documentos acessórios foram incluídos na pasta `sections` (ex.: guia de migração da v1 para v2).
- [ ] Precisamos atualizar algo na documentação do projeto (`readme.md`)?

### Site e Comunicação

- [ ] Página do componente no site (`apps/site/docs/components`) foi criada/atualizada com todos os exemplos, readme e outros documentos acessórios.
- [ ] O site está renderizando normalmente após a criação/atualização da página do componente?
- [ ] Precisamos incluir alguma notícia no site (`apps/site/blog`) para informar a comunidade?

### Build

- [ ] O `build` da biblioteca `stencil` está sendo realizado com sucesso.
- [ ] O `build` da biblioteca `angular` está sendo realizado com sucesso.
- [ ] O `build` da biblioteca `react` está sendo realizado com sucesso.
- [ ] O `build` da biblioteca `vue` está sendo realizado com sucesso.
- [ ] O `build` do site está sendo realizado com sucesso.

### Acessibilidade

- [ ] Testes de acessibilidade foram realizados e o componente está em conformidade.
