<!-- markdownlint-disable MD041 -->

## Site

### Build

- [ ] O `build` do site está sendo realizado com sucesso.

### Padrões e Renderização

- [ ] As páginas criadas ou atualizadas seguem os padrões das páginas já existentes.
- [ ] O site está sendo renderizado corretamente após as alterações.

### Controle de Versão

- [ ] Uma versão foi gerada manualmente? Se sim, qual o motivo?

### Acessibilidade

- [ ] As páginas criadas ou atualizadas estão acessíveis conforme os padrões de acessibilidade.
- [ ] Todos os links nas páginas estão funcionando corretamente.
