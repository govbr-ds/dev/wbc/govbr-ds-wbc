<!-- markdownlint-disable MD041 -->

## Bots

- [ ] Configurações dos dois bots `.triage-policies.yml` e `Danger` foram conferidas para evitar conflitos?
- [ ] Bots foram testados em branchs, mrs e ou ambientes controlados para evitar problemas generalizados com o repositório?
- [ ] Documentação das partes criadas/alteradas foi criadas?

### Funcionamento

- [ ] A documentação dos bots está atualizada.
