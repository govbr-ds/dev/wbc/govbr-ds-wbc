<!-- markdownlint-disable MD041 -->

## Exemplos dos componentes

- [ ] Arquivo `packages/webcomponents/src/index.html` foi atualizado com exemplos do componente.
- [ ] Arquivos de exemplos foram criados na pasta `packages/webcomponents/src/pages/{component}`.
- [ ] Arquivos de exemplos estão seguindo o padrão de organização dos outros componentes.
- [ ] Arquivo `packages/webcomponents/src/pages/{component}/{component}.js` foi atualizado com os eventos do componente.
- [ ] Os exemplos criados fazem sentidos para a nossa biblioteca?

### Documentação

- [ ] A documentação dos exemplos foi atualizada.
- [ ] Os exemplos estão documentados de forma clara e concisa.
