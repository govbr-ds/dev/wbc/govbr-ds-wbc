<!-- markdownlint-disable MD041 -->

## Documento 'Como migrar'

- [ ] O documento segue o padrão definido pelos outros documentos do projeto.
- [ ] Contém exemplos de como o componente era usado na v1 e como deverá ser usado na v2.
- [ ] Contém comparações detalhadas entre as `props` da v1 e da v2.
- [ ] Contém comparações entre os eventos da v1 e da v2.
- [ ] Contém comparações entre os métodos da v1 e da v2.

### Atualização

- [ ] O documento de migração está atualizado com as últimas mudanças.
- [ ] O documento de migração segue o padrão estabelecido.
