image: node:20.18.1-alpine

cache:
  paths:
    - node_modules
    - public

variables:
  HUSKY: 0

stages:
  - install
  # - build
  # - quality
  # - bots
  - release
  - publish

.prepare-pnpm: &prepare-pnpm
  - apk --no-cache add bash git
  - npm install -g corepack@latest
  - corepack enable
  - corepack use pnpm@9.15.5
  - corepack prepare pnpm@9.15.5 --activate
  - pnpm config set store-dir .pnpm-store

install_dependencies:
  stage: install
  interruptible: true
  artifacts:
    paths:
      - node_modules
  script:
    - *prepare-pnpm
    - pnpm install
  rules:
    - when: always

# build_project:
#   stage: build
#   interruptible: true
#   artifacts:
#     paths:
#       - dist
#   script:
#     - *prepare-pnpm
#     - pnpm dlx nx run-many -t build --verbose
#   rules:
#     - when: always

# spellcheck:
#   stage: quality
#   script:
#     - *prepare-pnpm
#     - pnpm run spellcheck:ci
#   artifacts:
#     paths:
#       - report-spellcheck.txt
#     when: always
#     reports:
#       dotenv: report-spellcheck.txt
#   rules:
#     - when: always

# unit_tests:
#   stage: quality
#   interruptible: true
#   artifacts:
#     paths:
#       - packages/webcomponents/coverage/
#     reports:
#       junit:
#         - clover.xml
#       coverage_report:
#         coverage_format: cobertura
#         path: packages/webcomponents/coverage/clover.xml
#     expire_in: 1 week
#     expose_as: "Unit Tests Report"
#   coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
#   script:
#     - *prepare-pnpm
#     - pnpm dlx nx test:unit webcomponents
#   rules:
#     - when: always

# e2e_tests:
#   stage: quality
#   interruptible: true
#   script:
#     - apk --no-cache add chromium chromium-chromedriver nss
#     - mkdir -p /opt/google/chrome
#     - ln -sf /usr/bin/chromium-browser /opt/google/chrome/chrome
#     - *prepare-pnpm
#     - pnpm dlx nx test:e2e webcomponents
#   rules:
#     - when: always

# eslint:
#   stage: quality
#   interruptible: true
#   artifacts:
#     paths:
#       - report-eslint.html
#     expire_in: 1 week
#     expose_as: "ESLINT Report"
#   script:
#     - *prepare-pnpm
#     - pnpm dlx nx lint:tsx webcomponents --outputFile=report-eslint.html --format=html
#   rules:
#     - when: always

# markdown_lint_webcomponents:
#   stage: quality
#   interruptible: true
#   artifacts:
#     when: always
#     paths:
#       - apps/**/*/report-markdown.txt
#       - packages/**/*/report-markdown.txt
#       - report-markdown.txt
#     expire_in: 1 week
#   script:
#     - *prepare-pnpm
#     - pnpm dlx nx lint:md webcomponents --output=report-markdown.txt
#   rules:
#     - when: always

# markdown_lint_site:
#   stage: quality
#   interruptible: true
#   artifacts:
#     when: always
#     paths:
#       - apps/**/*/report-markdown.txt
#       - packages/**/*/report-markdown.txt
#       - report-markdown.txt
#     expire_in: 1 week
#   script:
#     - *prepare-pnpm
#     - pnpm dlx nx lint:md site --output=report-markdown.txt
#   rules:
#     - when: always

# commit_lint:
#   stage: quality
#   interruptible: true
#   artifacts:
#     when: always
#     paths:
#       - report-commitlint.txt
#     expire_in: 1 week
#     expose_as: "Commitlint Report"
#   script:
#     - *prepare-pnpm
#     - pnpm dlx commitlint-gitlab-ci -x @govbr-ds/commitlint-config > report-commitlint.txt
#   rules:
#     - when: always

semantic_release:
  stage: release
  interruptible: true
  script:
    - *prepare-pnpm
    - npm config set //registry.npmjs.org/:_authToken ${NPM_TOKEN}
    - pnpm dlx semantic-release
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $should_release == "true"'
      when: always
    - if: '$CI_PIPELINE_SOURCE != "schedule" && $CI_PIPELINE_SOURCE != "merge_request_event" && $should_release == "true"'
      when: manual

pages:
  stage: publish
  interruptible: true
  artifacts:
    paths:
      - public
  script:
    - |
      if [ -d public/mr ]; then
        mv public/mr mr_temp
      fi

      if [ -d public ]; then
        find public -mindepth 1 -maxdepth 1 ! -name 'mr' -exec rm -rf {} +
      fi

      mv dist/site/* public

      if [ -d mr_temp ]; then
        mkdir -p public/mr
        mv mr_temp/* public/mr
        rm -rf mr_temp
      fi

      if [ "$CI_PIPELINE_SOURCE" = "merge_request_event" ]; then
        sed -i 's|href="/|href="/bibliotecas/wbc/govbr-ds-wbc/mr/'"$CI_MERGE_REQUEST_IID"'/|g' dist/webcomponents/www/index.html
        sed -i 's|src="/|src="/bibliotecas/wbc/govbr-ds-wbc/mr/'"$CI_MERGE_REQUEST_IID"'/|g' dist/webcomponents/www/index.html

        mkdir -p public/mr/$CI_MERGE_REQUEST_IID
        cp -r dist/webcomponents/www/* public/mr/$CI_MERGE_REQUEST_IID/
      fi
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" || $CI_COMMIT_REF_NAME == "main" || $CI_COMMIT_REF_NAME == "next"'
      when: always
# .ruby-cache: &ruby-cache
#   cache:
#     key:
#       files:
#         - Gemfile.lock
#     paths:
#       - vendor/ruby
#     policy: pull

# .use-ruby:
#   image: ruby:3.2
#   <<: *ruby-cache
#   variables:
#     BUNDLE_FROZEN: "true"
#     BUNDLE_PATH: "vendor"
#   before_script:
#     - ruby --version
#     - gem install bundler --no-document
#     - bundle --version
#     - bundle install --jobs $(nproc) --retry 3 --quiet
#     - bundle check

# dry_run_triage:
#   stage: bots
#   interruptible: true
#   extends: .use-ruby
#   script:
#     - gem install gitlab-triage
#     - gitlab-triage --help
#     - gitlab-triage --dry-run -r ./.triage-custom.rb --token $TRIAGE_GITLAB_API_TOKEN --source groups --source-id govbr-ds/bibliotecas/wbc
#   rules:
#     - if: '$run_bots == "true"'

# run_triage:
#   stage: bots
#   interruptible: true
#   extends: .use-ruby
#   script:
#     - gem install gitlab-triage
#     - gitlab-triage -r ./.triage-custom.rb --token $GITLAB_API_TOKEN --source groups --source-id govbr-ds/bibliotecas/wbc
#   rules:
#     - if: '$run_bots == "true"'

# run_danger:
#   image: ruby:3.2
#   interruptible: true
#   stage: bots
#   before_script:
#     - bundle install
#   script:
#     - bundle exec danger
#   rules:
#     - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $run_bots == "true"'
#       when: always
