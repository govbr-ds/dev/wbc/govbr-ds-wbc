// Função para ordenar as tabelas em um conteúdo Markdown
export const sortTablesInMarkdown = (markdownContent: string): string => {
  const linebreak = getLineBreakChar(markdownContent)
  return markdownContent.replace(linebreak.regex, (a) => sortTable(a, linebreak.char))
}

function sortTable(tableString: string, linebreak: string) {
  const [header, hr, ...lines] = tableString.split(linebreak)
  if (lines.length === 0 || !/[\s|:-]*/.test(hr)) return tableString
  const endingWhitespace = lines.pop()
  if (endingWhitespace && endingWhitespace.trim() !== '') {
    lines.push(endingWhitespace)
    const sortedLines = [...lines].sort((a, b) => a.localeCompare(b))
    return [header, hr, ...sortedLines].join(linebreak)
  }
  const sortedLines = [...lines].sort((a, b) => a.localeCompare(b))
  return [header, hr, ...sortedLines, endingWhitespace].join(linebreak)
}

function getLineBreakChar(string: string) {
  const indexOfLF = string.indexOf('\n', 1)
  if (indexOfLF === -1) {
    if (string.indexOf('\r') !== -1) return { char: '\r', regex: /((\|[^|\r]*)+\|\r?)+/g }
    return { char: '\n', regex: /((\|[^|\n]*)+\|\n?)+/g }
  }
  if (string[indexOfLF - 1] === '\r') return { char: '\r\n', regex: /((\|[^|\r\n]*)+\|(\r\n)?)+/g }
  return { char: '\n', regex: /((\|[^|\n]*)+\|\n?)+/g }
}
