import { JsonDocsEvent } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

// Função para gerar a documentação em Markdown dos eventos de um componente
export const eventsToMarkdown = (events: JsonDocsEvent[]) => {
  const content: string[] = []

  // Se não houver eventos, retorna um array vazio
  if (events.length === 0) return content

  // Adiciona o título da seção de eventos
  content.push(`## Eventos`)
  content.push(``)

  const table = new MarkdownTable() // Cria uma nova tabela Markdown

  // Adiciona o cabeçalho da tabela
  table.addHeader(['Evento', 'Descrição', 'Tipo'])

  // Itera sobre cada evento e adiciona uma linha à tabela
  events.forEach((ev) => {
    table.addRow([`\`${ev.event}\``, getDocsField(ev), `\`CustomEvent<${ev.detail}>\``])
  })

  // Adiciona o conteúdo da tabela ao conteúdo geral
  content.push(...table.toMarkdown())
  content.push(``) // Linha em branco para formatação

  return content
}

// Função auxiliar para obter a documentação do campo de evento
const getDocsField = (prop: JsonDocsEvent) => {
  // Se o evento está `deprecated`, adiciona uma mensagem de depreciação
  return `${
    prop.deprecation !== undefined
      ? `<span style="color:red">**[Descontinuado/Obsoleto]**</span> ${prop.deprecation}<br/><br/>`
      : ''
  }${prop.docs}`
}
