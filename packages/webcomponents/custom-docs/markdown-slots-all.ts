import { JsonDocs } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

export const allSlotsToMarkdown = (cmps: JsonDocs) => {
  const content: string[] = []

  const table = new MarkdownTable() // Cria uma nova tabela Markdown

  // Adiciona o cabeçalho da tabela
  table.addHeader(['Slot', 'Componente', 'Descrição'])

  // Itera sobre os metadados das propriedades
  for (const cmp of cmps.components) {
    for (const slot of cmp.slots) {
      table.addRow([`\`"${slot.name}"\``, cmp.tag, slot.docs])
    }
  }

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)
  return content // Retorna o conteúdo gerado
}
