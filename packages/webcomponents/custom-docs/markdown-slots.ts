import { JsonDocsSlot } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

/**
 * Converte uma lista de metadados de Slots em uma tabela escrita em Markdown
 * @param slots os metadados dos Slots a serem convertidos
 * @returns uma lista de strings que compõem a tabela em Markdown
 */
export const slotsToMarkdown = (slots: JsonDocsSlot[]): ReadonlyArray<string> => {
  const content: string[] = []

  // Verifica se há slots a serem processados
  if (slots.length === 0) return content

  content.push(`## Slots`) // Adiciona o título da seção
  content.push(``)

  const table = new MarkdownTable() // Cria uma nova tabela Markdown
  table.addHeader(['Slot', 'Descrição']) // Adiciona o cabeçalho da tabela

  // Itera sobre os metadados dos slots
  slots.forEach((style) => {
    // Adiciona uma linha na tabela para cada slot
    table.addRow([style.name === '' ? '' : `\`"${style.name}"\``, style.docs])
  })

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)

  return content // Retorna o conteúdo gerado
}
