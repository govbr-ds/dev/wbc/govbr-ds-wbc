import * as fs from 'fs'
import * as path from 'path'

import { JsonDocsUsage } from '@stencil/core/internal'

// Função para ler os arquivos markdown de uma pasta
export const sectionsToMarkdown = (componentTag: string, basePath: string): JsonDocsUsage => {
  const usageData: JsonDocsUsage = {}

  const directoryPath = path.resolve(
    basePath,
    `packages/webcomponents/src/components/${componentTag.replace('br-', '')}/sections`
  )

  // Verifica se a pasta existe antes de tentar ler os arquivos
  if (!fs.existsSync(directoryPath)) {
    return usageData // Retorna um objeto vazio se a pasta não existir
  }

  // Lê o conteúdo da pasta especificada
  const files = fs.readdirSync(directoryPath)

  // Filtra os arquivos que terminam com ".md" e processa cada um
  files.forEach((file) => {
    const filePath = path.join(directoryPath, file)
    const fileExtension = path.extname(file)

    // Verifica se o arquivo é markdown
    if (fileExtension === '.md') {
      const fileNameWithoutExtension = path.basename(file, fileExtension)
      const fileContent = fs.readFileSync(filePath, 'utf8')
      usageData[fileNameWithoutExtension] = fileContent
    }
  })

  return usageData
}
