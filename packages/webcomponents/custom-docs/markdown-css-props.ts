import { JsonDocsStyle } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

// Função para gerar a documentação em Markdown a partir dos estilos CSS customizados
export const stylesToMarkdown = (styles: JsonDocsStyle[]) => {
  const content: string[] = []

  // Se não houver estilos, retorna um array vazio
  if (styles.length === 0) return content

  // Adiciona o título da seção de propriedades customizadas de CSS
  content.push(`## Propriedades Customizadas de CSS`)
  content.push(``) // Linha em branco para formatar o Markdown

  const table = new MarkdownTable() // Cria uma nova tabela em Markdown
  table.addHeader(['Nome', 'Descrição']) // Cabeçalhos da tabela

  // Para cada estilo, adiciona uma linha com o nome e a descrição
  styles.forEach((style) => {
    table.addRow([`\`${style.name}\``, style.docs])
  })

  // Adiciona o conteúdo da tabela em formato Markdown
  content.push(...table.toMarkdown())
  content.push(``) // Linha em branco no final para formatar o Markdown

  return content // Retorna o conteúdo gerado em formato de Markdown
}
