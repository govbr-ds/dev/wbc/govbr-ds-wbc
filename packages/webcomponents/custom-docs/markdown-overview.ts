/**
 * Gera uma seção 'Visão Geral' para um arquivo markdown
 * @param overview uma string de comentário de nível de componente a ser colocada em um arquivo markdown
 * @returns A seção de Visão Geral gerada. Se a visão geral fornecida estiver vazia, retorna uma lista vazia
 */
export const overviewToMarkdown = (overview: string | undefined): ReadonlyArray<string> => {
  // Retorna um array vazio se a visão geral estiver vazia
  if (overview === undefined || overview.trim() === '') return []

  const content: string[] = []
  content.push(`## Visão Geral`) // Adiciona o título da seção
  content.push('')
  content.push(`${overview.trim()}`) // Adiciona a visão geral, removendo espaços em branco desnecessários
  content.push('')

  return content
}
