import { JsonDocsPart } from '@stencil/core/internal'

import { MarkdownTable } from './docs-utils'

/**
 * Converte uma lista de metadados de Shadow Parts em uma tabela escrita em Markdown
 * @param parts os metadados dos Shadow Parts a serem convertidos
 * @returns uma lista de strings que compõem a tabela Markdown
 */
export const partsToMarkdown = (parts: JsonDocsPart[]): ReadonlyArray<string> => {
  const content: string[] = []

  // Retorna uma lista vazia se não houver partes
  if (parts.length === 0) return content

  content.push(`## Shadow Parts`) // Adiciona o título da seção
  content.push(``)

  const table = new MarkdownTable() // Cria uma nova tabela Markdown
  table.addHeader(['Part', 'Description']) // Adiciona o cabeçalho da tabela

  // Itera sobre os metadados das partes
  parts.forEach((style) => {
    // Adiciona uma linha na tabela para cada parte
    table.addRow([style.name === '' ? '' : `\`"${style.name}"\``, style.docs])
  })

  // Adiciona a tabela convertida ao conteúdo
  content.push(...table.toMarkdown())
  content.push(``)

  return content // Retorna o conteúdo gerado
}
