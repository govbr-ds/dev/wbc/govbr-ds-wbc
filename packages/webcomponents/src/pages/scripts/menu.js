const MENU_ITEMS = [
  {
    path: 'avatar',
    name: 'Avatar',
    files: ['photographic', 'iconic', 'letter'],
  },
  {
    path: 'button',
    name: 'Button',
    files: ['color-mode', 'density', 'emphasis', 'icon-position', 'shape', 'state'],
  },
  {
    path: 'checkbox',
    name: 'Checkbox',
    files: ['hidden-label', 'horizontal', 'indeterminate', 'slotted', 'state', 'vertical'],
  },
  {
    name: 'Checkgroup',
    path: 'checkgroup',
    files: ['checkgroup', 'concatenation'],
  },
  {
    path: 'dropdown',
    name: 'Dropdown',
    files: ['simples'],
  },
  {
    path: 'icon',
    name: 'Icon',
    files: ['flip', 'font-awesome', 'inline', 'rotate', 'width-height'],
  },
  {
    path: 'input',
    name: 'Input',
    files: ['state', 'with-button', 'with-message', 'highlight', 'inline', 'density', 'propriedades'],
  },
  {
    path: 'item',
    name: 'Item',
    files: ['density', 'vertical', 'state', 'selectable'],
  },
  {
    path: 'list',
    name: 'List',
    files: ['selectable', 'density', 'grouped', 'horizontal'],
  },
  {
    path: 'loading',
    name: 'Loading',
    files: ['indeterminate', 'progress'],
  },
  {
    path: 'message',
    name: 'Message',
    files: ['feedback', 'message'],
  },
  {
    path: 'radio',
    name: 'Radio',
    files: ['hidden-label', 'radio-group', 'slotted', 'state'],
  },
  {
    path: 'select',
    name: 'Select',
    files: ['simple', 'multiple'],
  },
  {
    path: 'switch',
    name: 'Switch',
    files: ['label', 'has-icon', 'density', 'state'],
  },
  {
    path: 'tag',
    name: 'Tag',
    files: ['tag', 'density', 'status', 'interaction', 'persistent-interaction', 'icons', 'counts'],
  },
  {
    path: 'textarea',
    name: 'Textarea',
    files: ['simples', 'contador', 'density', 'inline'],
  },
  {
    path: 'upload',
    name: 'Upload',
    files: ['single', 'multiple', 'restricted', 'disabled'],
  },
  // NOVOS COMPONENTES - NÃO DELETAR
]

const MENU_STATIC_ITEMS = [
  {
    path: 'all-component-events',
    name: 'Eventos',
  },
  {
    path: 'all-component-methods',
    name: 'Métodos',
  },
  {
    path: 'all-component-props',
    name: 'Propriedades',
  },
  {
    path: 'all-component-slots',
    name: 'Slots',
  },
]

const MENU_BODY = document.querySelector('.menu-body')
const MENU_FILTER = document.getElementById('menu-filter')
const CLEAR_MENU_FILTER = document.getElementById('clear-menu-filter')
const menuButton = document.getElementById('menu-button')
const responsiveMenuButton = document.getElementById('responsive-menu-button')
const menuElement = document.getElementById('main-navigation')
const iconElement = menuButton.querySelector('i')

function setLinks() {
  const baseUrl = getBaseUrl()

  MENU_ITEMS.sort(compareByNames).forEach((page) => {
    const link = document.createElement('a')
    link.href = `${baseUrl}/${page.path}`
    link.className = 'menu-item br-link'
    link.textContent = page.name
    link.addEventListener('click', (event) => {
      event.preventDefault()

      if (link.classList.contains('active')) {
        return
      }

      removeAllActiveClasses()
      link.classList.add('active')
      loadPage(page.path, page.files)
      window.history.pushState({}, '', `${baseUrl}/${page.path}`)

      if (window.innerWidth <= 575) {
        toggleMenu()
      }
    })

    MENU_BODY.appendChild(link)
  })

  const divider = document.createElement('div')
  divider.classList.add('br-divider')
  MENU_BODY.appendChild(divider)

  const staticHeader = document.createElement('div')
  staticHeader.classList.add('menu-item')
  staticHeader.innerHTML = 'Arquivos MD (precisa do <strong>build</strong>)'
  MENU_BODY.appendChild(staticHeader)

  MENU_STATIC_ITEMS.forEach((page) => {
    const link = document.createElement('a')
    link.href = `${baseUrl}/${page.path}`
    link.className = 'menu-item br-link'
    link.textContent = page.name
    link.addEventListener('click', (event) => {
      event.preventDefault()

      if (link.classList.contains('active')) {
        return
      }

      removeAllActiveClasses()
      link.classList.add('active')
      loadMarkdown(page.path)
      window.history.pushState({}, '', `${baseUrl}/${page.path}`)

      if (window.innerWidth <= 575) {
        toggleMenu()
      }
    })

    MENU_BODY.appendChild(link)
  })
}

function filterMenu() {
  const filterValue = MENU_FILTER.value.toLowerCase()
  const menuItems = MENU_BODY.querySelectorAll('.menu-item')

  menuItems.forEach((item) => {
    if (item.textContent.toLowerCase().includes(filterValue)) {
      item.style.display = ''
    } else {
      item.style.display = 'none'
    }
  })

  CLEAR_MENU_FILTER.style.display = filterValue ? 'block' : 'none'
}

function clearInput() {
  MENU_FILTER.value = ''
  filterMenu()
}

window.addEventListener('load', () => {
  setLinks()
  loadPageFromURL()

  menuButton.addEventListener('click', toggleMenu)
  responsiveMenuButton.addEventListener('click', toggleMenu)

  const mediaQuery = window.matchMedia('(min-width: 576px)')
  mediaQuery.addEventListener('change', handleMediaChange)

  function handleMediaChange(event) {
    if (event.matches && responsiveMenuButton) responsiveMenuButton.style.display = 'none'
    if (!event.matches && responsiveMenuButton) responsiveMenuButton.style.display = ''
  }

  handleMediaChange(mediaQuery)

  MENU_FILTER.addEventListener('input', filterMenu)
  CLEAR_MENU_FILTER.addEventListener('click', clearInput)
})

function removeAllActiveClasses() {
  const menuItems = document.querySelectorAll('.menu-item')
  menuItems.forEach((item) => {
    item.classList.remove('active')
  })
}

function toggleMenu() {
  menuElement.classList.toggle('active')

  if (menuElement.classList.contains('active')) {
    iconElement.classList.remove('fa-bars')
    iconElement.classList.add('fa-times')
  } else {
    iconElement.classList.add('fa-bars')
    iconElement.classList.remove('fa-times')
  }
}

function compareByNames(a, b) {
  if (a.name < b.name) {
    return -1
  }
  if (a.name > b.name) {
    return 1
  }
  return 0
}
