import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Prop } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'
/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/message?tab=designer).
 *
 * @slot default - Utilizado para inserir qualquer conteúdo que será exibido no corpo da mensagem.
 */
@Component({
  tag: 'br-message',
  styleUrl: 'message.scss',
  shadow: true,
  formAssociated: true,
})
export class Message {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrMessageElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Define o título da mensagem, que é exibido no início, acima da mensagem principal.
   * Este título serve para destacar a mensagem textual. Não é aplicável para mensagens do tipo feedback.
   */
  @Prop({ reflect: true }) readonly messageTitle: string

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-message-${messageId++}`

  /**
   * Define o texto da mensagem que será exibido.
   * Este é o conteúdo principal da mensagem. A propriedade `message` é obrigatória e deve ser fornecida para que a mensagem apareça.
   */
  @Prop({ reflect: true }) readonly message!: string

  /**
   * Se definido como verdadeiro, o título da mensagem será exibido na mesma linha que a mensagem principal.
   * Isso pode ser útil para criar um layout onde o título e a mensagem estão alinhados horizontalmente.
   */
  @Prop({ reflect: true }) readonly isInline: boolean = false

  /**
   * Se definido como verdadeiro, um botão de fechar será exibido para permitir que o usuário feche a mensagem.
   * O fechamento emitirá o evento `brDidClose`, mas não removerá automaticamente o componente do DOM, a menos que a propriedade `autoRemove` também esteja definida como `true`.
   * Este recurso não está disponível para mensagens do tipo feedback.
   */
  @Prop({ reflect: true }) readonly isClosable: boolean = false

  /**
   * Controla o comportamento do fechamento do componente quando `isClosable` é verdadeiro.
   * - Se definido como `true`, o componente será automaticamente removido do DOM ao clicar no botão de fechar.
   * - Se definido como `false`, o componente permanecerá no DOM e apenas emitirá o evento `brDidClose`.
   *
   * Esta propriedade não tem efeito se `isClosable` for `false`.
   * O valor padrão é `false`.
   */
  @Prop({ reflect: true }) autoRemove: boolean = false

  /**
   * Se definido como verdadeiro, um ícone associado à mensagem será exibido.
   * Use esta propriedade para mostrar ou ocultar o ícone da mensagem conforme necessário.
   */
  @Prop({ reflect: true }) readonly showIcon: boolean = false

  /**
   * Define se a mensagem é do tipo feedback, geralmente usada para fornecer contexto adicional sobre ações do usuário.
   * Exemplos incluem mensagens de validação em campos de formulário. Não disponível para mensagens que não sejam de feedback.
   */
  @Prop({ reflect: true }) readonly isFeedback: boolean = false

  /**
   * Define o estado do message.
   * Os possíveis valores são:
   * - 'info': Mensagem informativa.
   * - 'warning': Mensagem de aviso.
   * - 'danger': Mensagem de erro ou alerta.
   * - 'success': Mensagem de sucesso.
   * O valor padrão é 'info'.
   */
  @Prop({ reflect: true }) readonly state: 'info' | 'warning' | 'danger' | 'success' = 'info'

  /** Evento emitido quando o usuário fecha a mensagem, se closable for true. */
  @Event() brDidClose: EventEmitter

  private readonly dismissHandler = () => {
    this.brDidClose.emit()
    if (this.autoRemove) {
      this.el.remove()
    }
  }

  private checkIconState() {
    const iconMap = {
      success: 'fa-solid:check-circle',
      warning: 'fa-solid:exclamation-triangle',
      danger: 'fa-solid:times-circle',
      info: 'fa-solid:info-circle',
    }
    return iconMap[this.state] || 'check-circle' // Valor padrão se não encontrado
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-message': true,
      [this.state]: true,
    }
  }

  render() {
    return (
      <Host>
        {this.isFeedback ? (
          <div class="mb-3">
            <span class={`feedback ${this.state}`} role="alert" id={this.customId}>
              {this.showIcon ? <br-icon class="icon-space" icon-name={this.checkIconState()}></br-icon> : null}
              {this.message && this.message.length > 0 ? this.message : <slot></slot>}
            </span>
          </div>
        ) : (
          <div class={this.getCssClassMap()} role="alert" id={this.customId}>
            {this.showIcon && (
              <div class="icon">
                <br-icon icon-name={this.checkIconState()} height="20" aria-hidden="true"></br-icon>
              </div>
            )}
            <div class="content" role="alert">
              {this.message && this.message.length > 0 ? (
                <div>
                  {this.messageTitle && <span class="message-title mr-1">{this.messageTitle}</span>}
                  {this.messageTitle && !this.isInline && <br />}
                  {this.message && <span class="message-body">{this.message}</span>}
                </div>
              ) : (
                <div>
                  {this.messageTitle && this.isInline && <strong>{this.messageTitle}</strong>}
                  {this.messageTitle && !this.isInline && <br />}
                  <slot></slot>
                </div>
              )}
            </div>
            {this.isClosable && !this.isFeedback && (
              <div class="close">
                <br-button shape="circle" type="button" aria-label="Fechar a mensagem" onClick={this.dismissHandler}>
                  <br-icon icon-name="fa-solid:times" aria-hidden="true"></br-icon>
                </br-button>
              </div>
            )}
          </div>
        )}
      </Host>
    )
  }
}

let messageId = 0
