## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/message?tab=designer).

## Propriedades

| Propriedade               | Atributo        | Descrição                                                                                                                                                                                                                                                                                                                                                                                     | Tipo                                           | Valor padrão                    |
| ------------------------- | --------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ------------------------------- |
| `autoRemove`              | `auto-remove`   | Controla o comportamento do fechamento do componente quando `isClosable` é verdadeiro. - Se definido como `true`, o componente será automaticamente removido do DOM ao clicar no botão de fechar. - Se definido como `false`, o componente permanecerá no DOM e apenas emitirá o evento `brDidClose`.  Esta propriedade não tem efeito se `isClosable` for `false`. O valor padrão é `false`. | `boolean`                                      | `false`                         |
| `customId`                | `custom-id`     | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                                                                                                                                                                                        | `string`                                       | ```br-message-${messageId++}``` |
| `isClosable`              | `is-closable`   | Se definido como verdadeiro, um botão de fechar será exibido para permitir que o usuário feche a mensagem. O fechamento emitirá o evento `brDidClose`, mas não removerá automaticamente o componente do DOM, a menos que a propriedade `autoRemove` também esteja definida como `true`. Este recurso não está disponível para mensagens do tipo feedback.                                     | `boolean`                                      | `false`                         |
| `isFeedback`              | `is-feedback`   | Define se a mensagem é do tipo feedback, geralmente usada para fornecer contexto adicional sobre ações do usuário. Exemplos incluem mensagens de validação em campos de formulário. Não disponível para mensagens que não sejam de feedback.                                                                                                                                                  | `boolean`                                      | `false`                         |
| `isInline`                | `is-inline`     | Se definido como verdadeiro, o título da mensagem será exibido na mesma linha que a mensagem principal. Isso pode ser útil para criar um layout onde o título e a mensagem estão alinhados horizontalmente.                                                                                                                                                                                   | `boolean`                                      | `false`                         |
| `message` *(obrigatório)* | `message`       | Define o texto da mensagem que será exibido. Este é o conteúdo principal da mensagem. A propriedade `message` é obrigatória e deve ser fornecida para que a mensagem apareça.                                                                                                                                                                                                                 | `string`                                       | ---                             |
| `messageTitle`            | `message-title` | Define o título da mensagem, que é exibido no início, acima da mensagem principal. Este título serve para destacar a mensagem textual. Não é aplicável para mensagens do tipo feedback.                                                                                                                                                                                                       | `string`                                       | ---                             |
| `showIcon`                | `show-icon`     | Se definido como verdadeiro, um ícone associado à mensagem será exibido. Use esta propriedade para mostrar ou ocultar o ícone da mensagem conforme necessário.                                                                                                                                                                                                                                | `boolean`                                      | `false`                         |
| `state`                   | `state`         | Define o estado do message. Os possíveis valores são: - 'info': Mensagem informativa. - 'warning': Mensagem de aviso. - 'danger': Mensagem de erro ou alerta. - 'success': Mensagem de sucesso. O valor padrão é 'info'.                                                                                                                                                                      | `"danger" \| "info" \| "success" \| "warning"` | `'info'`                        |

## Eventos

| Evento       | Descrição                                                               | Tipo               |
| ------------ | ----------------------------------------------------------------------- | ------------------ |
| `brDidClose` | Evento emitido quando o usuário fecha a mensagem, se closable for true. | `CustomEvent<any>` |


## Slots

| Slot        | Descrição                                                                       |
| ----------- | ------------------------------------------------------------------------------- |
| `"default"` | Utilizado para inserir qualquer conteúdo que será exibido no corpo da mensagem. |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-message
    state="success"
    is-feedback
    message="Campo correto."
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message
    state="warning"
    is-feedback
    message="A tecla CAPS-LOCK está ativada."
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message
    state="danger"
    is-feedback
    message="O CPF deve conter apenas dígitos."
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message state="info" is-feedback show-icon aria-label="Mensagem do aria-label para acessibilidade">
    <span>Os arquivos devem ser no formato PNG, JPG, PDF e ter no máximo 1GB.</span>
  </br-message>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-message
    state="success"
    message-title="Success!"
    message="Campo correto."
    is-inline
    is-closable
    auto-remove
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message
    state="warning"
    message-title="Warning!"
    is-inline
    auto-remove
    message="This is a warning message."
    is-closable
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message
    state="danger"
    message-title="Error!"
    is-inline
    message="Something went wrong."
    is-closable
    auto-remove
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  ></br-message>
  <br-message
    message-title="Informação."
    is-inline
    is-closable
    auto-remove
    show-icon
    aria-label="Mensagem do aria-label para acessibilidade"
  >
    <span>Seus dados só serão salvos após o preenchimento do primeiro campo do formulário.</span>
  </br-message>
</div>
```
## Dependências

### Depende de

- [br-icon](../icon)
- [br-button](../button)

### Gráfico
```mermaid
graph TD;
  br-message --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-message --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-message mainComponent
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                                            | Tipo      | Padrão   |
| --------------- | ------------------- | ------------------------------------------------------------------------------------ | --------- | -------- |
| `closable`      | `isClosable`        | Habilita o botão de fechar a mensagem. Opcional                                      | `boolean` | `false`  |
| `feedback`      | `isFeedback`        | Mensagem contextual de feedback. Opcional                                            | `boolean` | `false`  |
| `inline`        | `isInline`          | Exibir o título na mesma linha que a mensagem. Opcional                              | `boolean` | `false`  |
| `showIcon`      | `showIcon`          | Exibir ícone na mensagem. Opcional                                                   | `boolean` | `false`  |
| `text`          | `message`           | Texto da mensagem. **Obrigatório**                                                   | `string`  | `''`     |
| `title`         | `messageTitle`      | Título da mensagem. Opcional                                                         | `string`  | `''`     |
| `type`          | `state`             | Estado da mensagem. **Obrigatório**. Valores: 'info', 'warning', 'danger', 'success' | `string`  | `'info'` |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-message></br-message>` no Vue e em StencilJS:

#### Vue

- Evento `close`: Evento emitido quando a mensagem é fechada.

Exemplo:

```html
<br-message id="message-exemplo" text="Mensagem de exemplo"></br-message>
```

Adicionando o listener de evento:

```javascript
document.getElementById('message-exemplo').addEventListener('close', (e) => {
  console.log('Mensagem fechada')
})
```

#### StencilJS

- Usando eventos em JSX: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidClose`, a propriedade será chamada `onBrDidClose`.

```html
<!-- StencilJS -->
<br-message onBrDidClose="handleMessageClose"></br-message>
```

- Ouvindo eventos de um elemento não JSX:

```html
<template>
  <br-message></br-message>
</template>

<script>
  const messageElement = document.querySelector('br-message')
  messageElement.addEventListener('onBrDidClose', (event) => {
    /* seu listener */
  })
</script>
```

| Evento                    | Descrição                                                                                                 |
| ------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate` | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDidClose`              | Evento emitido quando o usuário fecha a mensagem, se closable for true.                                   |
| `brDisconnectedCallback`  | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |

## Métodos

Não se aplica.

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão.

Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Message em uma aplicação Web.

```html
<template>
  <br-message
    id="message-01"
    state="success"
    message-title="Título da Mensagem"
    message="Texto da mensagem de exemplo"
    is-inline
    is-closable
    show-icon
    onBrDidClose="handleMessageClose"
  ></br-message>
</template>
<script>
  export default {
    methods: {
      handleMessageClose(event) {
        console.log('Mensagem fechada: ', event)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).