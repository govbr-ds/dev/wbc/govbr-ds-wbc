import { AttachInternals, Component, Element, h, Host, Prop } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/button?tab=designer).
 *
 * @slot default - Espaço de conteúdo do button. Geralmente usado para incluir o rótulo.
 */
@Component({
  tag: 'br-button',
  styleUrl: 'button.scss',
  shadow: true,
  formAssociated: true,
})
export class Button {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrButtonElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Define se o botão usará um esquema de cores escuro.
   */
  @Prop({ reflect: true }) readonly colorMode: 'dark'

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-button-${buttonId++}`

  /**
   * Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.
   */
  @Prop({ reflect: true }) readonly density: 'large' | 'medium' | 'small' = 'medium'

  /**
   * Desativa o botão, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Define a ênfase do botão, alterando sua aparência para criar hierarquia visual e destacar ações importantes.
   */
  @Prop({ reflect: true }) readonly emphasis: 'primary' | 'secondary' | 'tertiary'

  /**
   * Indica se o botão está no estado ativo.
   * Se definido como verdadeiro, o botão será exibido como ativo.
   */
  @Prop({ reflect: true }) readonly isActive: boolean = false

  /**
   * Aplica o estado de "progresso" ao botão.
   * O botão exibirá um indicador de carregamento ou progresso.
   */
  @Prop({ reflect: true }) readonly isLoading: boolean = false

  /**
   * Define o formato do botão.
   * Valores possíveis:
   * - 'circle': Botão com formato circular.
   * - 'block': Botão com formato ovalado ocupando toda a largura disponível.
   * - 'pill': Botão com formato ovalado, com bordas arredondadas.
   */
  @Prop({ reflect: true }) readonly shape: 'circle' | 'block' | 'pill'

  /**
   * Define o tipo de botão, especificando seu comportamento padrão.
   * Valores possíveis:
   * - 'button': Um botão padrão.
   * - 'reset': Um botão para redefinir o formulário.
   * - 'submit': Um botão para enviar o formulário.
   */
  @Prop({ reflect: true }) readonly type: 'button' | 'reset' | 'submit'

  /**
   * Define o valor inicial do botão em um formulário.
   */
  @Prop({ reflect: true }) readonly value: string
  private readonly handleClick = () => {
    if (this.type === 'submit') {
      const submitEvent = new SubmitEvent('submit', {
        submitter: this.elementInternals.form,
        composed: true,
        cancelable: true,
        bubbles: true,
      })
      this.elementInternals.form.dispatchEvent(submitEvent)
    }

    if (this.type === 'reset') {
      this.elementInternals.form.reset()
    }
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-button': true,
      primary: this.emphasis === 'primary',
      secondary: this.emphasis === 'secondary',
      large: this.density === 'large',
      small: this.density === 'small',
      block: this.shape === 'block',
      circle: this.shape === 'circle',
      active: this.isActive,
      loading: this.isLoading,
      'dark-mode': this.colorMode === 'dark',
    }
  }

  render() {
    return (
      <Host aria-disabled={this.disabled ? 'true' : null}>
        <button
          class={this.getCssClassMap()}
          type={this.type}
          id={this.customId}
          disabled={this.disabled}
          value={this.value}
          onClick={this.handleClick}
        >
          <slot></slot>
        </button>
      </Host>
    )
  }
}

let buttonId = 0
