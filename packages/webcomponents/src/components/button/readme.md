## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/button?tab=designer).

## Propriedades

| Propriedade | Atributo     | Descrição                                                                                                                                                                                                                    | Tipo                                     | Valor padrão                  |
| ----------- | ------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- | ----------------------------- |
| `colorMode` | `color-mode` | Define se o botão usará um esquema de cores escuro.                                                                                                                                                                          | `"dark"`                                 | ---                           |
| `customId`  | `custom-id`  | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                       | `string`                                 | ```br-button-${buttonId++}``` |
| `density`   | `density`    | Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.                                                                                                                          | `"large" \| "medium" \| "small"`         | `'medium'`                    |
| `disabled`  | `disabled`   | Desativa o botão, tornando-o não interativo.                                                                                                                                                                                 | `boolean`                                | `false`                       |
| `emphasis`  | `emphasis`   | Define a ênfase do botão, alterando sua aparência para criar hierarquia visual e destacar ações importantes.                                                                                                                 | `"primary" \| "secondary" \| "tertiary"` | ---                           |
| `isActive`  | `is-active`  | Indica se o botão está no estado ativo. Se definido como verdadeiro, o botão será exibido como ativo.                                                                                                                        | `boolean`                                | `false`                       |
| `isLoading` | `is-loading` | Aplica o estado de "progresso" ao botão. O botão exibirá um indicador de carregamento ou progresso.                                                                                                                          | `boolean`                                | `false`                       |
| `shape`     | `shape`      | Define o formato do botão. Valores possíveis: - 'circle': Botão com formato circular. - 'block': Botão com formato ovalado ocupando toda a largura disponível. - 'pill': Botão com formato ovalado, com bordas arredondadas. | `"block" \| "circle" \| "pill"`          | ---                           |
| `type`      | `type`       | Define o tipo de botão, especificando seu comportamento padrão. Valores possíveis: - 'button': Um botão padrão. - 'reset': Um botão para redefinir o formulário. - 'submit': Um botão para enviar o formulário.              | `"button" \| "reset" \| "submit"`        | ---                           |
| `value`     | `value`      | Define o valor inicial do botão em um formulário.                                                                                                                                                                            | `string`                                 | ---                           |



## Slots

| Slot        | Descrição                                                             |
| ----------- | --------------------------------------------------------------------- |
| `"default"` | Espaço de conteúdo do button. Geralmente usado para incluir o rótulo. |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-button id="active" emphasis="primary" color-mode="dark">Ênfase Primária</br-button>
  <br-button id="active" emphasis="secondary" color-mode="dark">Ênfase Secundária</br-button>
  <br-button id="active" color-mode="dark">Ênfase Terciária</br-button>
  <br-button id="active" emphasis="primary" is-active color-mode="dark">Ativo</br-button>
  <br-button id="active" emphasis="primary" disabled color-mode="dark">Desabilitado</br-button>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-button id="small" emphasis="secondary" density="small">Densidade Alta</br-button>
  <br-button id="medium" emphasis="primary">Densidade Padrão</br-button>
  <br-button id="large" emphasis="secondary" density="large">Densidade Baixa</br-button>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-button id="primary" emphasis="primary">Primária</br-button>
  <br-button id="secondary" emphasis="secondary">Secundária</br-button>
  <br-button id="tertiary">Terciária</br-button>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-button emphasis="primary">
    <br-icon icon-name="fa6-solid:city"></br-icon>
    Ícone à esquerda
  </br-button>
  <br-button emphasis="secondary">
    Ícone à Direita
    <br-icon icon-name="fa6-solid:phone"></br-icon>
  </br-button>
</div>
```
```html
<br-button id="block" emphasis="secondary" shape="block">Bloco</br-button>
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-button id="circle" emphasis="primary" shape="circle" aria-label="Ícone ilustrativo">
    <br-icon icon-name="fa6-solid:city"></br-icon>
  </br-button>
  <br-button id="block" emphasis="secondary" shape="pill">Pílula</br-button>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5">
  <br-button id="active" emphasis="secondary" is-active>Ativo</br-button>
  <br-button id="disabled" emphasis="secondary" disabled>Desabilitado</br-button>
  <br-button id="loading" emphasis="secondary" is-loading>Loading</br-button>
</div>
```
## Dependências

### Usado por

 - [br-input](../input)
 - [br-message](../message)
 - [br-tag](../tag)
 - [br-upload](../upload)

### Gráfico
```mermaid
graph TD;
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-message --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-tag --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-upload --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-button mainComponent
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

#### Propriedades retiradas na versão 2.x

| Propriedade | Descrição                                    | Tipo      | Padrão  |
| ----------- | -------------------------------------------- | --------- | ------- |
| `block`     | Usado para dar o formato de bloco ao botão   | `boolean` | `false` |
| `circle`    | Usado para dar o formato de círculo ao botão | `boolean` | `false` |
| `type`      | Usado para definir a ênfase do botão         | `string`  | `null`  |

#### Propriedades incluídas na versão 2.x

| Propriedade | Descrição                  | Tipo                               | Padrão      |
| ----------- | -------------------------- | ---------------------------------- | ----------- |
| `emphasis`  | Informa a ênfase do botão  | `primary \| secondary \| tertiary` | `tertiary`  |
| `shape`     | Informa o formato do botão | `'circle \| block \| pill`         | `pill`      |
| `type`      | Informa o tipo do botão    | `button \| reset \| submit`        | `undefined` |

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).