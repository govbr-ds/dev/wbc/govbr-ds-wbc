## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/input?tab=designer).

## Propriedades

| Propriedade    | Atributo       | Descrição                                                                                                                                                                                                                                                                                                                                                                     | Tipo                                                                                                                    | Valor padrão                |
| -------------- | -------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- | --------------------------- |
| `autocomplete` | `autocomplete` | Controla o comportamento de preenchimento automático do navegador para o input.                                                                                                                                                                                                                                                                                               | `"off" \| "on"`                                                                                                         | ---                         |
| `autocorrect`  | `autocorrect`  | Controla a correção automática do texto.                                                                                                                                                                                                                                                                                                                                      | `"off" \| "on"`                                                                                                         | `'off'`                     |
| `customId`     | `custom-id`    | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado.                                                                                                                                                                                                                                                                                        | `string`                                                                                                                | ```br-input-${inputId++}``` |
| `density`      | `density`      | Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.                                                                                                                                                                                                                                                                           | `"large" \| "medium" \| "small"`                                                                                        | `'medium'`                  |
| `disabled`     | `disabled`     | Desativa o input, tornando-o não interativo.                                                                                                                                                                                                                                                                                                                                  | `boolean`                                                                                                               | `false`                     |
| `helpText`     | `help-text`    | Texto adicional que fornece ajuda ou informações sobre o input.                                                                                                                                                                                                                                                                                                               | `string`                                                                                                                | ---                         |
| `isHighlight`  | `is-highlight` | Se verdadeiro, o input terá destaque visual.                                                                                                                                                                                                                                                                                                                                  | `boolean`                                                                                                               | `false`                     |
| `isInline`     | `is-inline`    | Se verdadeiro, o rótulo e o input estarão na mesma linha (layout inline).                                                                                                                                                                                                                                                                                                     | `boolean`                                                                                                               | `false`                     |
| `label`        | `label`        | Texto exibido como rótulo do input.                                                                                                                                                                                                                                                                                                                                           | `string`                                                                                                                | ---                         |
| `max`          | `max`          | Define o valor máximo para campos de entrada numéricos.                                                                                                                                                                                                                                                                                                                       | `number`                                                                                                                | ---                         |
| `maxlength`    | `maxlength`    | Define o comprimento máximo do valor do campo de entrada.                                                                                                                                                                                                                                                                                                                     | `number`                                                                                                                | ---                         |
| `min`          | `min`          | Define o valor mínimo para campos de entrada numéricos.                                                                                                                                                                                                                                                                                                                       | `number`                                                                                                                | ---                         |
| `minlength`    | `minlength`    | Define o comprimento mínimo do valor do campo de entrada.                                                                                                                                                                                                                                                                                                                     | `number`                                                                                                                | ---                         |
| `multiple`     | `multiple`     | Se verdadeiro, permite a seleção de múltiplos arquivos.                                                                                                                                                                                                                                                                                                                       | `boolean`                                                                                                               | `false`                     |
| `name`         | `name`         | Nome do input, utilizado para identificação em formulários.                                                                                                                                                                                                                                                                                                                   | `string`                                                                                                                | ---                         |
| `pattern`      | `pattern`      | Define o padrão de entrada para validação.                                                                                                                                                                                                                                                                                                                                    | `string`                                                                                                                | ---                         |
| `placeholder`  | `placeholder`  | Texto exibido dentro do input quando está vazio, fornecendo uma dica ou sugestão ao usuário.                                                                                                                                                                                                                                                                                  | `string`                                                                                                                | ---                         |
| `readonly`     | `readonly`     | Se verdadeiro, o valor do input é exibido, mas não pode ser editado pelo usuário.                                                                                                                                                                                                                                                                                             | `boolean`                                                                                                               | `false`                     |
| `required`     | `required`     | Se verdadeiro, o input é obrigatório e deve ser preenchido antes que o formulário possa ser enviado.                                                                                                                                                                                                                                                                          | `boolean`                                                                                                               | `false`                     |
| `state`        | `state`        | Define o estado do input Os possíveis valores são: - 'info': Mensagem informativa. - 'warning': Mensagem de aviso. - 'danger': Mensagem de erro ou alerta. - 'success': Mensagem de sucesso. O valor padrão é 'info'.                                                                                                                                                         | `"danger" \| "info" \| "success" \| "warning"`                                                                          | ---                         |
| `step`         | `step`         | Define o valor do passo para campos de entrada numéricos.                                                                                                                                                                                                                                                                                                                     | `number`                                                                                                                | ---                         |
| `type`         | `type`         | Especifica o tipo de entrada do campo. Pode ser um dos seguintes: - 'color': Seletor de cor - 'email': Campo para e-mail - 'hidden': Campo oculto - 'password': Campo para senha - 'range': Controle deslizante - 'search': Campo de pesquisa - 'number': Campo para números - 'tel': Campo para números de telefone - 'text': Campo de texto padrão - 'url': Campo para URLs | `"color" \| "email" \| "file" \| "hidden" \| "number" \| "password" \| "range" \| "search" \| "tel" \| "text" \| "url"` | `'text'`                    |
| `value`        | `value`        | Valor exibido no input. Pode ser alterado pelo usuário se a propriedade `readonly` não estiver ativa.                                                                                                                                                                                                                                                                         | `string`                                                                                                                | ---                         |

## Eventos

| Evento        | Descrição                 | Tipo                  |
| ------------- | ------------------------- | --------------------- |
| `valueChange` | Valor atualizado do input | `CustomEvent<string>` |


## Slots

| Slot          | Descrição                                                                                                                                      |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| `"action"`    | Botão à direita do input.                                                                                                                      |
| `"feedback"`  | Mensagem de feedback como resposta específica a uma interação do usuário com o input. Pode ser feedback de erro, aviso, sucesso ou informação. |
| `"help-text"` | Personalização do texto de ajuda.                                                                                                              |
| `"icon"`      | Ícone à esquerda do input.                                                                                                                     |



## Exemplos
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="small" label="Densidade alta" placeholder="Densidade alta" density="small" class="mr-2"></br-input>
  <br-input
    id="medium"
    label="Densidade padrão"
    placeholder="Densidade padrão"
    density="medium"
    class="mr-2"
  ></br-input>
  <br-input id="large" label="Densidade baixa" placeholder="Densidade baixa" density="large" class="mr-2"></br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="highlight" label="Destaque" placeholder="Destaque" is-highlight>
    <br-icon slot="icon" icon-name="fa-solid:user" aria-hidden="true"></br-icon>
    <br-icon slot="action" icon-name="fa-solid:search" aria-hidden="true"></br-icon>
  </br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="inline" label="Label / Rótulo" placeholder="Placeholder" is-inline></br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-between mt-5 p-1">
  <!-- Campo com Min e Max -->
  <br-input
    id="min-max"
    type="number"
    min="1"
    max="5"
    label="Campo com Min e Max (5 caracteres)"
    placeholder="Escolha um número entre 1 e 5"
    title="Limite de 1 a 5"
    density="large"
    class="mr-2"
  ></br-input>

  <!-- Campo com Autocorrect -->
  <br-input
    id="autocorrect"
    type="text"
    autocorrect="on"
    label="Campo com Autocorrect (correção automática)"
    placeholder="Digite seu texto com correção automática"
    title="Correção ortográfica ativada"
    density="large"
    class="mr-2"
    aria-describedby="autocorrect-description"
  ></br-input>

  <!-- Campo com MinLength e MaxLength -->
  <br-input
    id="min-max-length"
    type="text"
    minLength="1"
    maxLength="10"
    label="Campo com MinLength e MaxLength(10)"
    placeholder="Digite um texto de até 10 caracteres"
    title="Limite de 1 a 10 caracteres"
    density="large"
    class="mr-2"
  ></br-input>

  <!-- Campo com Pattern -->
  <br-input
    id="country-code-pattern"
    type="text"
    pattern="[A-Za-z]{3}"
    label="Campo com Pattern"
    placeholder="Digite o código de país com três letras"
    title="Formato de código de país de três letras (ex: BRA)"
    density="large"
    class="mr-2"
    required
    aria-required="true"
    aria-describedby="pattern-description"
  ></br-input>

  <!-- Campo de Nome de Usuário com Spellcheck -->
  <br-input
    type="text"
    id="username"
    name="username"
    label="Campo com Spellcheck (Correção Ortográfica)"
    placeholder="Digite seu nome de usuário"
    title="Verifique a ortografia do seu nome de usuário"
    spellcheck="true"
    aria-label="Nome de usuário com verificação ortográfica"
    density="large"
    class="mr-2"
    aria-describedby="username-description"
  ></br-input>
  <!-- Campo de Nome de Usuário com Step -->
  <br-input
    type="number"
    id="points"
    name="points"
    label="Campo com Step (incremento de 3)"
    placeholder="Digite a quantidade de pontos"
    step="3"
    title="Digite um número de pontos com incremento de 3"
    density="large"
    class="mr-2"
    required
    min="0"
    aria-label="Pontos com incremento de 3"
    aria-describedby="points-info"
  ></br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="disabled" label="Disabled" placeholder="Disabled" disabled class="mr-2"></br-input>
  <br-input
    id="success"
    label="Validação: sucesso"
    placeholder="Validação: sucesso"
    state="success"
    class="mr-2"
  ></br-input>
  <br-input
    id="danger"
    label="Validação: danger"
    placeholder="Validação: danger"
    state="danger"
    class="mr-2"
  ></br-input>
  <br-input id="info" label="Validação: info" placeholder="Validação: info" state="info" class="mr-2"></br-input>
  <br-input
    id="warning"
    label="Validação: warning"
    placeholder="Validação: warning"
    state="warning"
    class="mr-2"
  ></br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="button" label="Com botão à direita" placeholder="Com botão à direita">
    <br-icon slot="action" icon-name="fa-solid:search" height="16" aria-hidden="true"></br-icon>
  </br-input>
  <br-input
    label="Senha"
    type="password"
    placeholder="Placeholder"
    help-text="Informe a senha com 06 caracteres e no máximo 08 caracteres"
  >
  </br-input>
  <br-input id="icon" label="Com ícones" placeholder="Com ícones">
    <br-icon slot="icon" icon-name="fa-solid:user" aria-hidden="true"></br-icon>
  </br-input>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-input id="success01" label="Validação: sucesso" placeholder="Validação: sucesso" class="mr-2">
    <br-message
      slot="feedback"
      state="success"
      is-feedback
      message="Mensagem de sucesso"
      show-icon
      aria-label="Mensagem de sucesso"
    ></br-message>
  </br-input>
  <br-input id="danger01" label="Validação: danger" placeholder="Validação: danger" class="mr-2">
    <br-message
      slot="feedback"
      state="danger"
      is-feedback
      message="Mensagem de erro"
      show-icon
      aria-label="Mensagem de erro"
    ></br-message>
  </br-input>
  <br-input id="info01" label="Validação: info" placeholder="Validação: info" class="mr-2">
    <br-message
      slot="feedback"
      state="info"
      is-feedback
      message="Mensagem de info"
      show-icon
      aria-label="Mensagem de info"
    ></br-message>
  </br-input>
  <br-input id="warning01" label="Validação: warning" placeholder="Validação: warning" class="mr-2">
    <br-message
      slot="feedback"
      state="warning"
      is-feedback
      message="Mensagem de warning"
      show-icon
      aria-label="Mensagem de warning"
    ></br-message>
  </br-input>
</div>
```
## Dependências

### Usado por

 - [br-select](../select)

### Depende de

- [br-icon](../icon)
- [br-button](../button)

### Gráfico
```mermaid
graph TD;
  br-input --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-select --Depende---> br-input
  click br-input "src/components/input" "Link para a documentação do componente input"
  class br-input depComponent
  class br-input mainComponent
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil                 | Descrição                                                                                                      | Tipo      | Padrão      |
| --------------- | ----------------------------------- | -------------------------------------------------------------------------------------------------------------- | --------- | ----------- |
| `autocomplete`  | `autocomplete`                      | Define o comportamento do autocomplete no formulário.                                                          | `string`  | `undefined` |
| `autofocus`     | `autofocus`                         | Define se o campo de entrada recebe foco automaticamente ao carregar a página.                                 | `boolean` | `false`     |
| `button`        | `<br-icon slot="action"></br-icon>` | Define o botão à direita para o campo de entrada. Se `type="password"`, não há necessidade de definir o ícone. | `boolean` | `false`     |
| `danger`        | `danger`                            | Define se o campo de entrada está em estado de perigo.                                                         | `boolean` | `false`     |
| `density`       | `density`                           | Define a densidade do campo de entrada.                                                                        | `string`  | `''`        |
| `disabled`      | `disabled`                          | Define se o campo de entrada é desabilitado.                                                                   | `boolean` | `false`     |
| `highlight`     | `is-highlight`                      | Define se o campo de entrada está destacado.                                                                   | `boolean` | `false`     |
| `icon`          | `<br-icon slot="icon"></br-icon>`   | Define o ícone para o campo de entrada.                                                                        | `boolean` | `false`     |
| `info`          | `info`                              | Define se o campo de entrada está em estado de informação.                                                     | `boolean` | `false`     |
| `inline`        | `is-inline`                         | Define o layout do input e label como inline.                                                                  | `boolean` | `false`     |
| `label`         | `label`                             | Define o rótulo do campo de entrada.                                                                           | `string`  | `undefined` |
| `name`          | `name`                              | Define o nome do campo de entrada.                                                                             | `string`  | `undefined` |
| `placeholder`   | `placeholder`                       | Define o placeholder do campo de entrada.                                                                      | `string`  | `undefined` |
| `readonly`      | `readonly`                          | Define se o campo de entrada é somente leitura.                                                                | `boolean` | `false`     |
| `required`      | `required`                          | Define se o campo de entrada é obrigatório.                                                                    | `boolean` | `false`     |
| `success`       | `success`                           | Define se o campo de entrada está em estado de sucesso.                                                        | `boolean` | `false`     |
| `type`          | `type`                              | Define o tipo de campo de entrada.                                                                             | `string`  | `text`      |
| `value`         | `value`                             | Define o valor inicial do campo de entrada.                                                                    | `string`  | `undefined` |
| `warning`       | `warning`                           | Define se o campo de entrada está em estado de alerta.                                                         | `boolean` | `false`     |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-input></br-input>` no Vue e em StencilJS:

#### Vue

- Evento *`input`*: Evento emitido quando o valor do campo de entrada é modificado.

Exemplo:

```html
<br-input id="input-exemplo" label="Teste"> </br-input>
```

Adicionando o listener de evento:

```javascript
document.getElementById('input-exemplo').addEventListener('input', (e) => {
  const inputValue = e.target.value
  console.log('O valor do input é: ' + inputValue)
})
```

#### StencilJS

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `valueChange`, a propriedade será chamada `onValueChange`.

```html
<!-- StencilJS -->
<br-input onValueChange="handleEvent"></br-input>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-input></br-input>
</template>

<script>
  const inputElement = document.querySelector('br-input')
  inputElement.addEventListener('valueChange', (event) => {
    /* your listener */
  })
</script>
```

### Métodos

- `handleInput(event)`: é o método público acessível que atualiza o valor do campo de entrada e emite o evento de alteração. Veja como exemplo, o método é chamado dentro do listener de evento `onInput`:

```javascript
<script>const inputElement = document.querySelector('br-input'); inputElement.handleInput(event);</script>
```

### Slots

Foi criado o slot `icon` no StencilJS, que permite a personalização do ícone associado ao input.

```html
<!-- StencilJS -->
<br-input>
  <span slot="icon">🔍</span>
  <!-- Conteúdo do slot -->
</br-input>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Input em uma aplicação Web.

```html
<template>
  <br-input id="input-01" name="input-01" label="Nome" aria-label="nome" onValueChange="handleEvent"></br-input>
</template>
<script>
  export default {
    methods: {
      handleEvent(event) {
        console.log(event.detail)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).