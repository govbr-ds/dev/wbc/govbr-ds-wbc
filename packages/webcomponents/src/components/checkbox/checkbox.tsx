import {
  AttachInternals,
  Component,
  Element,
  Event,
  EventEmitter,
  h,
  Host,
  Method,
  Prop,
  State,
  Watch,
} from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/checkbox?tab=designer).
 *
 * @slot default - O texto descritivo pode ser passado por propriedade (label) ou por slot.
 */
@Component({
  tag: 'br-checkbox',
  styleUrl: 'checkbox.scss',
  shadow: true,
  formAssociated: true,
})
export class Checkbox {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrCheckboxElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Define o estado intermediário do checkbox.
   * Se definido como verdadeiro, o checkbox exibirá um estado intermediário, que é um estado visual que indica que a opção está parcialmente selecionada.
   * Este estado é útil quando o checkbox faz parte de um grupo com seleção parcial.
   * Este estado será resetado automaticamente ao clicar no checkbox, alterando para marcado ou desmarcado.
   */
  @Prop({ reflect: true, mutable: true }) indeterminate: boolean = false

  /**
   * Define o estado de seleção do checkbox.
   * Se definido como verdadeiro, o checkbox estará marcado. Caso contrário, estará desmarcado.
   */
  @Prop({ reflect: true, mutable: true }) checked: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-checkbox-${checkboxId++}`

  /**
   * Desativa o checkbox, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Define se o label associado ao checkbox deve ser oculto.
   * Se definido como verdadeiro, o texto do label será oculto, mas o checkbox ainda estará visível e funcional.
   */
  @Prop({ reflect: true }) readonly hasHiddenLabel: boolean = false

  /**
   * Texto descritivo exibido à direita do checkbox.
   * Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.
   */
  @Prop({ reflect: true }) readonly label: string

  /**
   * Define o nome do checkbox, que é utilizado para agrupar checkboxes em formulários e identificar o campo.
   * O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.
   */
  @Prop({ reflect: true }) readonly name!: string

  /**
   * Indica a validade do checkbox.
   * Valores possíveis:
   * 'valid': O checkbox é considerado válido.
   * 'invalid': O checkbox é considerado inválido.
   * Se não for especificado, o valor padrão é `null`, indicando que a validade não foi definida.
   */
  @Prop({ reflect: true }) readonly state: 'valid' | 'invalid'

  /**
   * Define o valor associado ao checkbox quando ele faz parte de um formulário nativo (`<form>`).
   * Esse valor é enviado com o formulário quando o checkbox está selecionado.
   * **Nota:** Esta propriedade não deve ser utilizada para determinar se o checkbox está selecionado; para verificar o estado de seleção, use a propriedade `checked`.
   */
  @Prop({ reflect: true }) readonly value: string

  @State() slotDefault: boolean = false

  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  @Event() checkedChange: EventEmitter<boolean>

  /**
   * Disparado depois que o valor do `indeterminate` foi alterado.
   */
  @Event() indeterminateChange: EventEmitter<boolean>

  @Watch('indeterminate')
  updateIndeterminate(newValue: boolean) {
    const input = this.el.shadowRoot.querySelector('input')
    if (input !== null) {
      input.indeterminate = newValue
    }
  }

  componentWillLoad() {
    this.hasSlotContentDefault()
  }

  private hasSlotContentDefault(): void {
    this.slotDefault = this.el.innerHTML !== ''
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-checkbox': true,
      valid: this.state === 'valid',
      invalid: this.state === 'invalid',
      disabled: this.disabled,
      'hidden-label': this.hasHiddenLabel,
    }
  }

  private readonly handleCheckedChange = () => {
    if (this.indeterminate) {
      // Sai do estado indeterminado ao clicar e define como marcado
      this.indeterminate = false
      this.checked = true
      this.indeterminateChange.emit(this.indeterminate)
    } else {
      // Alterna entre marcado e desmarcado
      this.checked = !this.checked
    }
    this.checkedChange.emit(this.checked)
  }

  /**
   * Define o estado indeterminado do checkbox.
   * @param value Novo valor para o estado indeterminado.
   */
  @Method()
  async setIndeterminate(value: boolean) {
    this.indeterminate = value
    this.updateIndeterminate(value)
    this.indeterminateChange.emit(this.indeterminate)
  }

  /**
   * Inverte o valor da prop `checked`
   */
  @Method()
  async toggleChecked() {
    this.checked = !this.checked
    this.checkedChange.emit(this.checked)
  }

  render() {
    return (
      <Host>
        <div class={this.getCssClassMap()}>
          <input
            type="checkbox"
            id={this.customId}
            name={this.name}
            checked={this.checked}
            disabled={this.disabled}
            value={this.value}
            onChange={this.handleCheckedChange}
            ref={(el) => el && (el.indeterminate = this.indeterminate)}
          />
          <label htmlFor={this.customId}>{this.slotDefault ? <slot></slot> : this.label}</label>
        </div>
      </Host>
    )
  }
}

let checkboxId = 0
