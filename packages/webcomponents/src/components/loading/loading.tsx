import { Component, Element, h, Host, Prop } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'
/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/loading?tab=designer).
 */
@Component({
  tag: 'br-loading',
  styleUrl: 'loading.scss',
  shadow: true,
  formAssociated: true,
})
export class Loading {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrLoadingElement

  /**
   * Determina o tipo de indicador a ser exibido:
   * - Se verdadeiro, exibirá uma barra de progresso com a porcentagem de progresso.
   * - Se falso, exibirá um indicador de carregamento indeterminado.
   */
  @Prop({ reflect: true }) readonly isProgress: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-loading-${loadingId++}`

  /**
   * Define a porcentagem de progresso a ser exibida na barra de progresso.
   * Deve ser um valor numérico entre 0 e 100. Ignorado se `isProgress` estiver definido como falso.
   */
  @Prop({ reflect: true }) readonly progressPercent: number

  /**
   * Se verdadeiro, ajusta o tamanho do indicador de carregamento para o tamanho médio.
   */
  @Prop({ reflect: true }) readonly isMedium: boolean = false

  /**
   * Texto a ser exibido abaixo do indicador de carregamento.
   * Pode ser usado para fornecer informações adicionais ou descritivas sobre o estado do carregamento.
   */
  @Prop({ reflect: true }) readonly label: string

  private getCssClassMap(): CssClassMap {
    return {
      'br-loading': true,
      medium: this.isMedium,
    }
  }

  render() {
    return (
      <Host>
        {this.isProgress ? (
          <div
            class={this.getCssClassMap()}
            role="progressbar"
            data-progress={this.progressPercent}
            aria-label="Carregando exemplo com porcentagem"
            aria-valuemin="0"
            aria-valuenow={this.progressPercent}
            aria-valuemax="100"
            id={this.customId}
          >
            <div class="br-loading-mask full">
              <div class="br-loading-fill"></div>
            </div>
            <div class="br-loading-mask">
              <div class="br-loading-fill"></div>
            </div>
          </div>
        ) : (
          <div class="loading-progress-alignment">
            <div
              id={this.customId}
              class={this.getCssClassMap()}
              role="progressbar"
              aria-label="Carregando exemplo indeterminado pequeno"
            ></div>
            <span class="rotulo">{this.label}</span>
          </div>
        )}
      </Host>
    )
  }
}

let loadingId = 0
