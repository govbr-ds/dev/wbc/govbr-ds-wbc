import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Method, Prop, State } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 *  Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/switch?tab=designer).
 *
 * @slot default - Descreve o que o switch faz quando a alternância estiver ativada. A label passada por slot tem precedência sobre a propriedade 'label'.
 */
@Component({
  tag: 'br-switch',
  styleUrl: 'switch.scss',
  shadow: true,
  formAssociated: true,
})
export class Switch {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrSwitchElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Desativa o switch, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Define o estado de seleção do checkbox.
   * Se definido como verdadeiro, o checkbox estará marcado. Caso contrário, estará desmarcado.
   */
  @Prop({ reflect: true, mutable: true }) checked: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-switch-${switchId++}`

  /**
   * Texto descritivo.
   * Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.
   */
  @Prop() readonly label: string

  /**
   * Posição do rótulo em relação ao switch.
   */
  @Prop({ reflect: true }) readonly labelPosition: 'right' | 'left' | 'top' = 'left'

  /**
   * Texto exibido quando o switch está ativado.
   */
  @Prop({ reflect: true }) readonly labelOn: string

  /**
   * Texto exibido quando o switch está desativado.
   */
  @Prop({ reflect: true }) readonly labelOff: string

  /**
   * Adiciona um ícone ao switch para indicar a mudança de estado.
   */
  @Prop({ reflect: true }) readonly hasIcon: boolean = false

  /**
   * Define o nome do switch, que é utilizado para agrupar switches em formulários e identificar o campo.
   * O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.
   */
  @Prop({ reflect: true }) readonly name: string

  /**
   * Define o valor associado ao switch quando ele faz parte de um formulário nativo (`<form>`).
   * Esse valor é enviado com o formulário quando o switch está selecionado.
   * **Nota:** Esta propriedade não deve ser utilizada para determinar se o switch está selecionado; para verificar o estado de seleção, use a propriedade `checked`.
   */
  @Prop({ reflect: true }) readonly value: string

  /** O ajuste da densidade consiste em aumentar ou reduzir a área de interação do switch. Quanto menor for a densidade, maior a área de interação. */
  @Prop({ reflect: true }) readonly density: 'large' | 'medium' | 'small' = 'medium'

  // Propriedade interna para verificar a presença de conteúdo no slot default
  @State() _slotDefault: boolean = false

  componentWillLoad() {
    this._hasSlotContentDefault()
  }

  /**
   * Disparado depois que o valor do `checked` foi alterado
   */

  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  @Event() checkedChange: EventEmitter<boolean>
  private _hasSlotContentDefault(): void {
    this._slotDefault = this.el.innerHTML !== ''
  }

  private readonly handleCheckedChange = (event: Event) => {
    this.checked = (event.target as HTMLInputElement).checked
    this.checkedChange.emit(this.checked)
  }

  private readonly handleClick = (event: Event) => {
    event.stopPropagation()
  }

  /**
   * Inverte o valor da prop `checked`
   */
  @Method()
  async toggleChecked() {
    this.checked = !this.checked
    this.checkedChange.emit(this.checked)
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-switch': true,
      icon: this.hasIcon,
      top: this.labelPosition === 'top',
      right: this.labelPosition === 'right',
      large: this.density === 'large',
      small: this.density === 'small',
    }
  }

  render() {
    return (
      <Host>
        <div class={this.getCssClassMap()}>
          <input
            id={this.customId}
            type="checkbox"
            name={this.name}
            checked={this.checked}
            disabled={this.disabled}
            role="switch"
            aria-checked={this.checked.toString()}
            aria-disabled={this.disabled ? 'true' : null}
            onChange={this.handleCheckedChange}
            onClick={this.handleClick}
          />
          <label htmlFor={this.customId}>{this._slotDefault ? <slot></slot> : this.label}</label>
          {this.labelOn && this.labelOff && (
            <div class="switch-data" data-enabled={this.labelOn} data-disabled={this.labelOff}></div>
          )}
        </div>
      </Host>
    )
  }
}

let switchId = 0
