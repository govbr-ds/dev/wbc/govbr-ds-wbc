import { Component, Element, h, Host, Prop, State, Watch } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/avatar?tab=designer).
 */
@Component({
  tag: 'br-avatar',
  styleUrl: 'avatar.scss',
  shadow: true,
})
export class Avatar {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrAvatarElement

  /**
   * Ajusta a densidade, alterando o espaçamento interno para um visual mais compacto ou mais expandido.
   */
  @Prop({ mutable: true }) density: 'small' | 'medium' | 'large' = 'medium'

  @Watch('density')
  handleDensityChange(newValue: 'small' | 'medium' | 'large') {
    if (!['small', 'medium', 'large'].includes(newValue)) {
      console.warn(`Valor inválido para density: ${newValue}. Usando 'medium'.`)
      this.density = 'medium'
    }
    this.el.setAttribute('density', this.density)
  }

  /**
   * Executa validações iniciais quando o componente é carregado
   */
  componentWillLoad() {
    this.handleDensityChange(this.density)
  }

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-avatar-${avatarId++}`

  /**
   * URL da imagem a ser exibida no avatar do tipo 'fotográfico'.
   * Deve ser uma URL válida que aponta para a imagem desejada.
   */
  @Prop() readonly src: string

  /**
   * Texto alternativo (alt) associado à imagem do avatar. Essencial para acessibilidade e SEO.
   * Deve descrever de forma clara e concisa o conteúdo da imagem, por exemplo: "Foto de perfil de João Silva".
   */
  @Prop() readonly alt = 'Imagem do avatar'

  /**
   * Indica se o avatar deve ser exibido como um ícone em vez de uma imagem fotográfica.
   * Se definido como verdadeiro, o avatar será exibido como um ícone.
   */
  @Prop() readonly isIconic: boolean = false

  /**
   * Texto exibido no avatar do tipo 'letra'.
   * Esse texto será mostrado em vez de uma imagem e pode ser usado para representar iniciais ou outras informações textuais.
   */
  @Prop() readonly text: string

  /**
   * Largura do ícone no avatar do tipo icônico
   * */
  @Prop() readonly iconWidth: string

  /**
   * Altura do ícone no avatar do tipo icônico
   */
  @Prop() readonly iconHeight: string

  /**
   * Ajuste para a margem de cima do ícone no avatar do tipo icônico
   */
  @Prop() readonly iconMarginTop: string

  /**
   * Define se o avatar está desabilitado
   */
  @Prop() readonly disabled = false

  @State() private hasError = false

  /**
   * Gera um mapa de classes CSS com base nas propriedades do componente.
   * O mapa de classes determina quais classes CSS devem ser aplicadas
   * ao elemento host do componente, ajustando sua aparência.
   *
   * @returns {CssClassMap} Objeto contendo as classes CSS aplicadas.
   */
  private getCssClassMap(): CssClassMap {
    return {
      'br-avatar': true,
      [this.density]: true,
      error: this.hasError,
      disabled: this.disabled,
    }
  }

  private readonly iconSizes = {
    small: { height: '32px', width: '40px' },
    medium: { height: '80px', width: '88px' },
    large: { height: '128px', width: '136px' },
  }

  private readonly iconMarginTops = {
    small: '0.7em',
    medium: '1em',
    large: '0.62em',
  }

  private readonly handleImageError = () => {
    this.hasError = true
    console.error(`Erro ao carregar imagem do avatar: ${this.src}`)
  }

  private renderAvatar() {
    if (this.text?.trim().length > 0) {
      return (
        <span class="content bg-support-01 text-pure-0" title={this.text}>
          {this.text[0].toUpperCase()}
        </span>
      )
    } else if (this.src?.trim().length > 0 && !this.hasError) {
      return (
        <span class="content" title={this.alt || this.text}>
          <img src={this.src} alt={this.alt} loading="lazy" onError={this.handleImageError} />
        </span>
      )
    } else {
      return (
        <span class="content" title={this.text}>
          <br-icon
            icon-name="fa-solid:user"
            height={this.iconHeight?.length > 0 ? this.iconHeight : this.iconSizes[this.density]?.height}
            width={this.iconWidth?.length > 0 ? this.iconWidth : this.iconSizes[this.density]?.width}
            aria-hidden="true"
            style={{
              'margin-top': this.iconMarginTop?.length > 0 ? this.iconMarginTop : this.iconMarginTops[this.density],
            }}
          ></br-icon>
        </span>
      )
    }
  }

  render() {
    return (
      <Host>
        <span id={this.customId} class={this.getCssClassMap()} aria-disabled={this.disabled}>
          {this.renderAvatar()}
        </span>
      </Host>
    )
  }
}

let avatarId = 0
