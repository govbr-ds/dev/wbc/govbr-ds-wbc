import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Method, Prop, Watch } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/dropdown?tab=designer).
 *
 * @slot trigger - Slot que recebe o elemento acionador
 * @slot target - Slot que recebe o elemento alvo
 */
@Component({
  tag: 'br-dropdown',
  styleUrl: 'dropdown.scss',
  shadow: true,
  formAssociated: true,
})
export class Dropdown {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrDropdownElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Indica se o dropdown está aberto ou fechado.
   * Esta propriedade é refletida no DOM e pode ser alterada externamente.
   * O valor padrão é falso (fechado).
   */
  @Prop({ reflect: true, mutable: true }) isOpen: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-dropdown-${dropdownId++}`

  /**
   * Método chamado sempre que a propriedade `isOpen` muda.
   * Emite um evento com o novo estado do dropdown.
   */
  @Watch('isOpen')
  isOpenChanged() {
    this.brDropdownChange.emit({ 'is-opened': this.isOpen })
  }

  connectedCallback() {
    document.addEventListener('click', this.onClickOutside.bind(this))
  }

  disconnectedCallback() {
    document.removeEventListener('click', this.onClickOutside.bind(this))
  }

  /**
   * Evento emitido quando o estado do dropdown muda.
   */
  @Event() brDropdownChange: EventEmitter<{ 'is-opened': boolean }>

  private getCssClassMap(): CssClassMap {
    return {
      'br-dropdown': true,
    }
  }

  /**
   * Alterna o estado do dropdown entre aberto e fechado.
   */
  private readonly toggle = (event: Event) => {
    event.stopPropagation()
    this.isOpen = !this.isOpen
  }

  /**
   * Método que lida com cliques fora do dropdown.
   *
   * @param {MouseEvent} event - O evento de clique que ocorreu.
   */
  private onClickOutside(event: MouseEvent) {
    const path = event.composedPath()
    const dropdownElement = this.el.shadowRoot.querySelector('#target')

    if (dropdownElement && !path.includes(dropdownElement)) {
      this.hide()
    }
  }

  /**
   * Abre o dropdown.
   * Define a propriedade `isOpen` como verdadeira e retorna o novo estado.
   * Este método é marcado com @Method() para que possa ser chamado externamente.
   */
  @Method()
  async open(): Promise<{ isOpen: boolean }> {
    this.isOpen = true
    return { isOpen: this.isOpen } // Retorna o novo estado
  }

  /**
   * Esconde o dropdown.
   * Define a propriedade `isOpen` como falsa e retorna o novo estado.
   * Este método é marcado com @Method() para que possa ser chamado externamente.
   */
  @Method()
  async hide(): Promise<{ isOpen: boolean }> {
    this.isOpen = false
    return { isOpen: this.isOpen } // Retorna o novo estado
  }

  render() {
    return (
      <Host is-open={this.isOpen}>
        <div class={this.getCssClassMap()} id={this.customId}>
          <div
            id="trigger"
            onClick={this.toggle}
            aria-controls="target"
            aria-haspopup="true"
            aria-expanded={this.isOpen}
          >
            <slot name="trigger"></slot>
          </div>
          <div id="target" aria-hidden={!this.isOpen}>
            <slot name="target"></slot>
          </div>
        </div>
      </Host>
    )
  }
}

let dropdownId = 0
