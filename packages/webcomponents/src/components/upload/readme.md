## Visão Geral

Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/upload?tab=designer).

## Propriedades

| Propriedade | Atributo   | Descrição                                                                                                                                                                                                                                                                                                                                | Tipo      | Valor padrão         |
| ----------- | ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | -------------------- |
| `accept`    | `accept`   | Tipos de arquivo permitidos (ex: 'image/*'). Esta propriedade define quais tipos de arquivos podem ser selecionados para upload.                                                                                                                                                                                                         | `string`  | `''`                 |
| `disabled`  | `disabled` | Indica se o componente está desativado. Quando definido como `true`, o campo de upload se torna não interativo, impedindo que o usuário selecione ou envie arquivos. Isso é útil para controlar a interação do usuário em situações específicas, como durante o carregamento de dados ou quando o formulário está em um estado inválido. | `boolean` | `false`              |
| `label`     | `label`    | Texto descritivo do campo de upload. Este é o texto que é mostrado no label do textarea.                                                                                                                                                                                                                                                 | `string`  | `'Envio de arquivo'` |
| `multiple`  | `multiple` | Indica se o componente permite a seleção de múltiplos arquivos. Quando definido como `true`, o usuário pode selecionar mais de um arquivo para upload.                                                                                                                                                                                   | `boolean` | `false`              |
| `state`     | `state`    | Define o estado visual da mensagem. Os possíveis valores são: - 'info': Mensagem informativa. - 'warning': Mensagem de aviso. - 'danger': Mensagem de erro ou alerta. - 'success': Mensagem de sucesso. O valor padrão é 'info'.                                                                                                         | `string`  | `UploadState.INFO`   |



## Slots

| Slot        | Descrição         |
| ----------- | ----------------- |
| `"default"` | Descrição do slot |



## Exemplos
```html
<div class="d-flex flex-column justify-content-evenly mt-5 p-4">
  <br-upload label="Envio de arquivo" disabled></br-upload>
  <br-message
    class="spacing-feedback"
    state="warning"
    is-feedback
    message="Upload desabilitado"
    show-icon
    aria-label="Upload desabilitado"
  ></br-message>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-upload label="Envio de arquivos" multiple></br-upload>
</div>
```
```html
<div class="d-flex flex-column justify-content-evenly mt-5 p-4">
  <br-upload label="Envio de arquivo" state="danger" accept="image/png, image/jpeg"></br-upload>
  <br-message
    class="spacing-feedback"
    state="danger"
    is-feedback
    message="Os arquivos devem ser no formato PNG ou JPG e ter no máximo 100MB."
    show-icon
    aria-label="Os arquivos devem ser no formato PNG ou JPG e ter no máximo 100MB."
  ></br-message>
</div>
```
```html
<div class="d-flex flex-wrap justify-content-evenly mt-5 p-4">
  <br-upload label="Envio de arquivo"></br-upload>
</div>
```
## Dependências

### Depende de

- [br-button](../button)
- [br-icon](../icon)

### Gráfico
```mermaid
graph TD;
  br-upload --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  br-upload --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  class br-upload mainComponent
```

---