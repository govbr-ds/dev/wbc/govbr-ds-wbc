import { newSpecPage } from '@stencil/core/testing'

import { Tag } from '../tag'

describe('br-tag', () => {
  it('deve renderizar a tag com as propriedades padrão', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag custom-id="br-tag-0"></br-tag>`,
    })

    // Tirar um snapshot do componente renderizado
    expect(page.root).toMatchSnapshot()

    expect(page.root).toEqualHtml(`
      <br-tag color="" custom-id="br-tag-0" density="small" icon-name="" label="" shape="default">
        <mock:shadow-root>
          <span class="br-tag small" id="br-tag-0">
            <span></span>
            <slot></slot>
          </span>
        </mock:shadow-root>
      </br-tag>
    `)
  })

  it('should render the tag with icon when showIcon is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag show-icon="true" icon-name="fa-solid:car"></br-tag>`,
    })

    const icon = page.root.shadowRoot.querySelector('br-icon')
    expect(icon).not.toBeNull()
    expect(icon.getAttribute('icon-name')).toBe('fa-solid:car')
  })

  it('should apply the color class when color prop is set', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag color="red"></br-tag>`,
    })

    expect(page.root.shadowRoot.querySelector('.br-tag').classList.contains('red')).toBe(true)
  })

  it('should render with different densities', async () => {
    const densities = ['small', 'medium', 'large']

    for (const density of densities) {
      const page = await newSpecPage({
        components: [Tag],
        html: `<br-tag density="${density}"></br-tag>`,
      })

      expect(page.root.shadowRoot.querySelector('.br-tag').classList.contains(density)).toBe(true)
    }
  })

  it('should render with rounded corners when rounded is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag shape="rounded"></br-tag>`,
    })

    expect(page.root.shadowRoot.querySelector('.br-tag').classList.contains('count')).toBe(true)
  })

  it('should render as circular when circle is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag shape="circle"></br-tag>`,
    })

    expect(page.root.shadowRoot.querySelector('.br-tag').classList.contains('icon')).toBe(true)
  })

  it('should render with close button when showCloseIcon is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag interaction></br-tag>`,
    })

    const closeButton = page.root.shadowRoot.querySelector('br-button')
    expect(closeButton).not.toBeNull()
    expect(closeButton.getAttribute('aria-label')).toBe('Fechar')
  })

  it('should render status label when status prop is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag status="true" label="Status"></br-tag>`,
    })

    const statusLabel = page.root.shadowRoot.querySelector('.tag-spacing-status')
    expect(statusLabel).not.toBeNull()
    expect(statusLabel.textContent).toBe('Status')
  })

  it('should not render label when circle is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag shape="circle" label="Test"></br-tag>`,
    })

    const label = page.root.shadowRoot.querySelector('#interactivetag1')
    expect(label).toBeNull()
  })

  it('should add interaction class when interaction prop is true', async () => {
    const page = await newSpecPage({
      components: [Tag],
      html: `<br-tag interaction="true"></br-tag>`,
    })

    expect(page.root.shadowRoot.querySelector('.br-tag').classList.contains('interaction')).toBe(true)
  })
})
