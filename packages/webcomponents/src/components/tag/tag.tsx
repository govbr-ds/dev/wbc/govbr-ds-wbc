import {
  AttachInternals,
  Component,
  Element,
  Event,
  EventEmitter,
  h,
  Host,
  Listen,
  Prop,
  State,
  Watch,
} from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 * Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/tag?tab=designer).
 *
 * @slot default - Espaço de conteúdo do tag. Geralmente usado para incluir o rótulo ou um ícone.
 */

// Constantes para valores fixos
const DENSITY_SMALL = 'small'
const DENSITY_MEDIUM = 'medium'
const DENSITY_LARGE = 'large'

const SHAPE_CIRCLE = 'circle'
const SHAPE_ROUNDED = 'rounded'
const SHAPE_DEFAULT = 'default'

@Component({
  tag: 'br-tag',
  styleUrl: 'tag.scss',
  shadow: true,
  formAssociated: true,
})
export class Tag {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrTagElement

  /** API de controle de formulário nativa */
  @AttachInternals() elementInternals: ElementInternals

  /**
   * A propriedade 'label' é uma string que representa o texto a ser exibido no componente.
   * Ela é refletida no DOM, permitindo que alterações no valor sejam refletidas no elemento HTML correspondente.
   */
  @Prop({ reflect: true }) readonly label: string = ''

  /**
   * A propriedade 'iconName' define o nome do ícone que será exibido ao lado do texto.
   * O valor padrão é 'fa-solid:car', que corresponde a um ícone de carro da biblioteca Font Awesome.
   */
  @Prop({ reflect: true }) readonly iconName: string = ''

  /**
   * A propriedade 'name' é uma string que representa o nome do grupo da tag para seleção.
   * Ela é refletida no DOM, permitindo que alterações no valor sejam refletidas no elemento HTML correspondente.
   */
  @Prop({ reflect: true }) readonly name: string = ''

  /**
   * A tag permite seleção múltipla.
   * */
  @Prop({ reflect: true }) readonly multiple: boolean = false

  /**
   * A tag está selecionada.
   * */
  @Prop({ reflect: true, mutable: true }) selected: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-tag-${tagId++}`

  /**
   * A propriedade 'color' permite definir a cor do componente.
   * Aceita valores como 'red', 'blue', entre outros, e é refletida no DOM.
   */
  @Prop({ reflect: true }) readonly color: string = ''

  /**
   * A propriedade 'density' permite definir a densidade do componente.
   * Aceita valores como 'small', 'medium' e 'large', e é refletida no DOM.
   */
  @Prop({ reflect: true }) readonly density: 'small' | 'medium' | 'large' = DENSITY_SMALL

  /**
   * A propriedade 'shape' define o formato do componente.
   * Valores possíveis:
   * - 'circle': Componente com formato circular.
   * - 'rounded': Componente com bordas arredondadas.
   * - 'default': Componente com formato padrão.
   */
  @Prop({ reflect: true }) readonly shape: 'circle' | 'rounded' | 'default' = SHAPE_DEFAULT

  /**
   * Status deve ser exibido.
   */
  @Prop({ reflect: true }) readonly status: boolean = false

  /**
   * Interação deve ser habilitada.
   */
  @Prop({ reflect: true, mutable: true }) interaction: boolean = false

  /**
   * Interação de seleção deve ser habilitada.
   * */
  @Prop({ reflect: true }) interactionSelect: boolean = false

  /**
   * Tag deve estar desabilitado.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Tag está selecionada.
   */
  @State() selectedClassValue: boolean = false

  /**
   * Evento emitido quando a tag é selecionada.
   */
  @Event() radioSelected: EventEmitter<string>

  @Watch('interaction')
  handleInteractionChange(newValue: boolean) {
    if (newValue && this.interactionSelect) {
      console.warn(
        "Aviso: 'interaction' e 'interactionSelect' não podem ser usados ao mesmo tempo. Alterando o 'interaction' para 'false'."
      )
      this.interaction = false
    }
  }

  @Watch('interactionSelect')
  handleInteractionSelectChange(newValue: boolean) {
    if (newValue && this.interaction) {
      console.warn(
        "Aviso: 'interaction' e 'interactionSelect' não podem ser usados ao mesmo tempo. Alterando o 'interaction' para 'false'."
      )
      this.interaction = false
    }
  }

  componentWillLoad() {
    if (this.interaction && this.interactionSelect) {
      console.warn(
        "Aviso: 'interaction' e 'interactionSelect' não podem ser usados ao mesmo tempo. Alterando o 'interaction' para 'false'."
      )
      this.interaction = false
    }
  }
  connectedCallback() {
    this.selectedClassValue = this.selected
  }

  @Watch('selected')
  handleSelectedChange(newValue: boolean) {
    this.selectedClassValue = newValue
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-tag': true,
      interaction: this.interaction,
      'interaction-select': this.interactionSelect,
      selected: this.selectedClassValue,
      [this.color]: !!this.color,
      [DENSITY_SMALL]: this.density === DENSITY_SMALL,
      [DENSITY_MEDIUM]: this.density === DENSITY_MEDIUM,
      [DENSITY_LARGE]: this.density === DENSITY_LARGE,
      count: this.shape === SHAPE_ROUNDED,
      icon: this.shape === SHAPE_CIRCLE,
      status: this.status,
    }
  }

  private renderIcon() {
    return this.iconName && this.iconName.trim() !== '' ? (
      <br-icon icon-name={this.iconName} aria-hidden="true" />
    ) : null
  }

  private renderLabel() {
    return this.shape !== SHAPE_CIRCLE && !this.status ? <span>{this.label}</span> : null
  }

  private renderCloseButton() {
    if (!this.interaction) return null
    return (
      <br-button
        type="button"
        shape="circle"
        emphasis="primary"
        aria-label="Fechar"
        onClick={this.handleClose}
        disabled={this.disabled}
      >
        <br-icon icon-name="fa-solid:times"></br-icon>
      </br-button>
    )
  }

  private renderStatusLabel() {
    return this.status ? <span class="tag-spacing-status">{this.label}</span> : null
  }

  private handleClose = () => {
    this.el.remove()
  }

  private handleChangeValueChecked = () => {
    if (!this.multiple) {
      this.radioSelected.emit(this.name)
    }
    this.selected = !this.selected
  }

  private handleChangeRadioValue = (event: MouseEvent) => {
    if (!this.multiple) {
      this.radioSelected.emit(this.name)
      this.selected = (event.target as HTMLInputElement).checked
    }
  }

  @Listen('radioSelected', { target: 'document' })
  handleCheckedChange(event: CustomEvent) {
    if (event.detail === this.name && this.el !== event.target) {
      this.selected = false
    }
  }

  render() {
    const mainTagContent = (
      <span class={this.getCssClassMap()} id={this.customId}>
        {this.renderIcon()}
        {this.renderLabel()}
        {this.renderCloseButton()}
        <slot></slot>
      </span>
    )

    return (
      <Host>
        {!this.status ? (
          mainTagContent
        ) : (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {mainTagContent}
            {this.renderStatusLabel()}
          </div>
        )}

        {this.interactionSelect && (
          <span class={this.getCssClassMap()} id={this.customId}>
            {this.multiple ? (
              <input
                id={this.label}
                type="checkbox"
                name={this.name}
                value={this.label}
                checked={this.selected}
                onClick={this.handleChangeValueChecked}
                disabled={this.disabled}
              />
            ) : (
              <input
                id={this.label}
                type="radio"
                name={this.name}
                value={this.label}
                checked={this.selected}
                onClick={this.handleChangeRadioValue}
                disabled={this.disabled}
              />
            )}
            <label htmlFor={this.label}>
              {this.renderIcon()}
              <span>{this.label}</span>
            </label>
          </span>
        )}
      </Host>
    )
  }
}

let tagId = 0
