import { AttachInternals, Component, Element, Event, EventEmitter, h, Host, Listen, Method, Prop } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

/**
 *  Para uma descrição detalhada, consulte a [documentação do GovBR-DS](https://www.gov.br/ds/components/radio?tab=designer).
 *
 * @slot default - O texto descritivo pode ser passado por propriedade (label) ou por slot.
 */

@Component({
  tag: 'br-radio',
  styleUrl: 'radio.scss',
  shadow: true,
  formAssociated: true,
})
export class Radio {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrRadioElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Define o estado de seleção do radio.
   * Se definido como verdadeiro, o radio estará marcado. Caso contrário, estará desmarcado.
   */
  @Prop({ reflect: true, mutable: true }) checked: boolean = false

  /**
   * Desativa o radio, tornando-o não interativo.
   */
  @Prop({ reflect: true }) readonly disabled: boolean = false

  /**
   * Indica a validade do radio.
   * Valores possíveis:
   * - 'valid': O radio é considerado válido.
   * - 'invalid': O radio é considerado inválido.
   * Se não for especificado, o valor padrão é `null`, indicando que a validade não foi definida.
   */
  @Prop({ reflect: true }) readonly state: 'valid' | 'invalid'

  /**
   * Define se o label associado ao radio deve ser oculto.
   * Se definido como verdadeiro, o texto do label será oculto, mas o radio ainda estará visível e funcional.
   */
  @Prop({ reflect: true, attribute: 'has-hidden-label' }) readonly hasHiddenLabel: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-radio-${radioId++}`

  /**
   * Define o nome do radio, que é utilizado para agrupar radios em formulários e identificar o campo.
   * O valor é obrigatório e deve ser fornecido para garantir o correto funcionamento em formulários.
   */

  @Prop({ reflect: true }) readonly name!: string

  /**
   * Texto descritivo exibido à direita do radio.
   * Caso um slot seja utilizado para fornecer um texto alternativo, o valor desta propriedade será ignorado.
   */
  @Prop({ reflect: true }) readonly label: string

  /**
   * Define o valor associado ao radio quando ele faz parte de um formulário nativo (`<form>`).
   * Esse valor é enviado com o formulário quando o radio está selecionado.
   * **Nota:** Esta propriedade não deve ser utilizada para determinar se o radio está selecionado; para verificar o estado de seleção, use a propriedade `checked`.
   */
  @Prop({ reflect: true }) readonly value: string

  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  // @Event({ bubbles: true, composed: true }) checkedChange: EventEmitter<{
  //   id: string;
  //   name: string;
  //   value: string;
  //   checked: boolean
  // }>
  @Event({ bubbles: true, composed: true }) checkedChange: EventEmitter<boolean>

  @Listen('checkedChange', { target: 'window' })
  handleCheckedChange(event: CustomEvent) {
    if (event.detail.id !== this.customId && event.detail.name === this.name) {
      this.checked = false
    }
  }

  /**
   * Inverte o valor da prop `checked`
   */
  @Method()
  async toggleChecked() {
    this.checked = !this.checked
    //this.checkedChange.emit({ id: this.customId, name: this.name, value: this.value, checked: this.checked })
    this.checkedChange.emit(this.checked)
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-radio': true,
      valid: this.state === 'valid',
      invalid: this.state === 'invalid',
      disabled: this.disabled,
      'hidden-label': this.hasHiddenLabel,
    }
  }

  /**
   * Handler para o event `click`
   * @param event Objeto Event
   */
  private readonly onClick = (event: MouseEvent) => {
    event.stopPropagation()
    this.checked = (event.target as HTMLInputElement).checked
  }

  /**
   * Handler para o evento `change`
   */
  private readonly onChange = () => {
    //this.checkedChange.emit({ id: this.customId, name: this.name, value: this.value, checked: this.checked })
    this.checkedChange.emit(this.checked)
  }

  render() {
    return (
      <Host aria-disabled={this.disabled ? 'true' : null}>
        <div class={this.getCssClassMap()}>
          <input
            part="radio-input"
            aria-label={this.label}
            type="radio"
            id={this.customId}
            name={this.name}
            checked={this.checked}
            disabled={this.disabled}
            value={this.value}
            onClick={this.onClick}
            onChange={this.onChange}
          />
          <label part="radio-label" htmlFor={this.customId}>
            <slot>{this.label}</slot>
          </label>
        </div>
      </Host>
    )
  }
}

let radioId = 0
