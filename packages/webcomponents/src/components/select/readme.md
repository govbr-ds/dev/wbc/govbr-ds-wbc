## Propriedades

| Propriedade        | Atributo             | Descrição                                                                              | Tipo      | Valor padrão                  |
| ------------------ | -------------------- | -------------------------------------------------------------------------------------- | --------- | ----------------------------- |
| `customId`         | `custom-id`          | Identificador único. Caso não seja fornecido, um ID gerado automaticamente será usado. | `string`  | ```br-select-${selectId++}``` |
| `isMultiple`       | `is-multiple`        | Habilita o modo múltiplo para selecionar várias opções.                                | `boolean` | `false`                       |
| `isOpen`           | `is-open`            | Indica se a listagem de itens do select está expandida                                 | `boolean` | `false`                       |
| `label`            | `label`              | Rótulo que indica o tipo de informação que deve ser selecionada.                       | `string`  | ---                           |
| `options`          | `options`            | JSON contendo as opções do select.                                                     | `string`  | `'[]'`                        |
| `placeholder`      | `placeholder`        | Texto auxiliar exibido antes de uma seleção.                                           | `string`  | `''`                          |
| `selectAllLabel`   | `select-all-label`   | Rótulo para selecionar todas as opções.                                                | `string`  | `'Selecionar todos'`          |
| `showSearchIcon`   | `show-search-icon`   | Exibe o ícone de busca no campo de entrada.                                            | `boolean` | `false`                       |
| `unselectAllLabel` | `unselect-all-label` | Rótulo para desmarcar todas as opções.                                                 | `string`  | `'Desselecionar todos'`       |

## Eventos

| Evento              | Descrição                                                            | Tipo               |
| ------------------- | -------------------------------------------------------------------- | ------------------ |
| `brDidSelectChange` | Evento emitido sempre que houver atualização nos itens selecionados. | `CustomEvent<any>` |

## Métodos

### `toggleOpen() => Promise<void>`

Inverte o valor da prop `isOpen`

#### Retorna

Tipo: `Promise<void>`







## Exemplos
```html
<div class="d-flex justify-content-evenly mt-5 p-4">
  <br-select
    label="Label"
    select-all-label="Marcar Todos"
    unselect-all-label="Desmarcar todos"
    show-search-icon
    is-multiple
    placeholder="Selecione uma opção"
    options='[
    { "label": "Opção 1", "value": "1", "selected": false },
    { "label": "Opção 2", "value": "2", "selected": false },
    { "label": "Opção 3", "value": "3", "selected": false }
  ]'
  ></br-select>
</div>
```
```html
<div class="d-flex justify-content-evenly mt-5 p-4">
  <br-select
    show-search-icon
    label="Label"
    placeholder="Selecione uma opção"
    options='[
    { "label": "Opção 1", "value": "1", "selected": false },
    { "label": "Opção 2", "value": "2", "selected": false },
    { "label": "Opção 3", "value": "3", "selected": false }
  ]'
  ></br-select>
</div>
```
## Dependências

### Depende de

- [br-input](../input)
- [br-icon](../icon)
- [br-checkbox](../checkbox)
- [br-radio](../radio)
- [br-list](../list)

### Gráfico
```mermaid
graph TD;
  br-select --Depende---> br-input
  click br-input "src/components/input" "Link para a documentação do componente input"
  class br-input depComponent
  br-select --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-select --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  br-select --Depende---> br-radio
  click br-radio "src/components/radio" "Link para a documentação do componente radio"
  class br-radio depComponent
  br-select --Depende---> br-list
  click br-list "src/components/list" "Link para a documentação do componente list"
  class br-list depComponent
  br-input --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  br-input --Depende---> br-button
  click br-button "src/components/button" "Link para a documentação do componente button"
  class br-button depComponent
  class br-select mainComponent
```

---
## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue    | Propriedade Stencil | Descrição                                                                                                  | Tipo      | Padrão                                                               |
| ------------------ | ------------------- | ---------------------------------------------------------------------------------------------------------- | --------- | -------------------------------------------------------------------- |
| --                 | `id`                | Especifica o ID do componente. Se não for fornecido, o ID será gerado automaticamente e de forma aleatória | `string`  | `undefined`                                                          |
| --                 | `isOpen`            | Indica se a listagem de itens do select está expandida.                                                    | `boolean` | `false`                                                              |
| `label`            | `label`             | Rótulo que indica o tipo de informação que deve ser selecionada.                                           | `string`  | `undefined`                                                          |
| `multiple`         | `isMultiple`        | Habilita o modo múltiplo para selecionar várias opções.                                                    | `boolean` | `false`                                                              |
| `options`          | `options`           | JSON contendo as opções disponíveis no select.                                                             | `object`  | `[{ "label": <string>, "value": <string>,  "selected": <boolean> }]` |
| `placeholder`      | `placeholder`       | Texto auxiliar exibido antes de uma seleção.                                                               | `string`  | `Selecione uma opção`                                                |
| `selectAllLabel`   | `selectAllLabel`    | Rótulo para selecionar todas as opções.                                                                    | `string`  | `"Selecionar todos"`                                                 |
| `showSearchIcon`   | `showSearchIcon`    | Exibe o ícone de busca no campo de entrada.                                                                | `boolean` | `false`                                                              |
| `unselectAllLabel` | `unselectAllLabel`  | Rótulo para desmarcar todas as opções.                                                                     | `string`  | `"Desselecionar todos"`                                              |

## Como Utilizar Eventos

Entenda melhor como utilizar os eventos para o componente `<br-select></br-select>` no Vue e em Stencil:

### Vue

| Evento   | Descrição                                   |
| -------- | ------------------------------------------- |
| `change` | Emitido ao alterar ou selecionar uma opção. |

### StencilJS

| Evento              | Descrição                                   |
| ------------------- | ------------------------------------------- |
| `brDidSelectChange` | Emitido ao alterar ou selecionar uma opção. |

- *Usando eventos em JSX*: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `brDidSelectChange`, a propriedade será chamada `onbrDidSelectChange`.

```html
<!-- StencilJS -->
<br-select onEventoPersonalizado="handleEvent"></br-select>
```

- *Ouvindo eventos de um elemento não JSX*: Ver exemplo abaixo:

```html
<template>
  <br-select></br-select>
</template>

<script>
  const selectElement = document.querySelector('br-select')
  selectElement.addEventListener('brDidSelectChange', (event) => {
    /* your listener */
  })
</script>
```

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).