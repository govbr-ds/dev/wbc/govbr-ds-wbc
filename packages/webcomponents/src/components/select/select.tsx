import { AttachInternals, Component, Element, Event, EventEmitter, h, Method, Prop, State, Watch } from '@stencil/core'

import { CssClassMap } from '../../utils/interfaces'

@Component({
  tag: 'br-select',
  styleUrl: 'select.scss',
  shadow: true,
  formAssociated: true,
})
export class Select {
  /**
   * Referência ao elemento host do componente.
   * Utilize esta propriedade para acessar e manipular o elemento do DOM associado ao componente.
   */
  @Element() el: HTMLBrSelectElement

  @AttachInternals() elementInternals: ElementInternals

  /**
   * Rótulo que indica o tipo de informação que deve ser selecionada.
   */
  @Prop({ reflect: true }) label: string

  /**
   * Texto auxiliar exibido antes de uma seleção.
   */
  @Prop({ reflect: true }) placeholder: string = ''

  /**
   * Habilita o modo múltiplo para selecionar várias opções.
   */
  @Prop({ reflect: true }) isMultiple: boolean = false

  /**
   * JSON contendo as opções do select.
   */
  @Prop({ reflect: true }) options: string = '[]'

  /**
   * Rótulo para selecionar todas as opções.
   */
  @Prop({ reflect: true }) selectAllLabel: string = 'Selecionar todos'

  /**
   * Rótulo para desmarcar todas as opções.
   */
  @Prop({ reflect: true }) unselectAllLabel: string = 'Desselecionar todos'

  /**
   * Exibe o ícone de busca no campo de entrada.
   */
  @Prop({ reflect: true }) showSearchIcon: boolean = false

  /**
   * Indica se a listagem de itens do select está expandida
   */
  @Prop({ reflect: true }) isOpen: boolean = false

  /**
   * Identificador único.
   * Caso não seja fornecido, um ID gerado automaticamente será usado.
   */
  @Prop({ reflect: true }) readonly customId: string = `br-select-${selectId++}`

  // Representa as opções do select disponíveis para renderização.
  // Cada opção contém `label`, `value` e o estado de seleção (`selected`).
  @State() parsedOptions: { label: string; value: string; selected: boolean }[] = []

  // Valor exibido no input, refletindo as opções selecionadas.
  @State() inputValue: string = ''

  // Indica se todas as opções estão selecionadas. Comportamento do checkbox "Selecionar Todos".
  @State() isAllSelected: boolean = false

  // Controla se a lista de opções do select está expandida (aberta) ou colapsada (fechada).
  @State() isExpanded: boolean = false

  // Armazena o índice do item atualmente focado, usado para navegação por teclado.
  @State() focusedIndex: number = -1

  /**
   * Evento emitido sempre que houver atualização nos itens selecionados.
   */
  @Event() brDidSelectChange: EventEmitter

  @Watch('options')
  synchronizeSelect() {
    try {
      const rawOptions = JSON.parse(this.options)
      this.parsedOptions = this.sanitizeOptions(rawOptions)
      this.updateSelectedOptions()
      this.updateSelectedAllState()
      this.emitSelectedOptions()
    } catch (error) {
      console.error('Erro ao processar as opções do select:', error)
      this.parsedOptions = []
    }
  }

  componentWillLoad() {
    this.isExpanded = this.isOpen
    this.synchronizeSelect()
  }

  componentDidLoad() {
    document.addEventListener('click', this.handleClickOutside)
  }

  disconnectedCallback() {
    document.addEventListener('click', this.handleClickOutside)
  }

  private sanitizeOptions(
    rawOptions: { label: string; value: string; selected: boolean }[]
  ): { label: string; value: string; selected: boolean }[] {
    const validOptions = []
    const invalidOptions = []

    rawOptions.forEach((option) => {
      if (typeof option.label === 'string' && option.label.trim() !== '') {
        validOptions.push({
          label: option.label.trim(),
          value: typeof option.value === 'string' ? option.value : '',
          selected: typeof option.selected === 'boolean' ? option.selected : false,
        })
      } else {
        invalidOptions.push(option)
      }
    })

    if (invalidOptions.length > 0) {
      console.warn('Opções inválidas descartadas:', invalidOptions)
    }

    return validOptions
  }

  private readonly handleClickOutside = (event: MouseEvent) => {
    const target = event.target as HTMLElement
    if (this.isExpanded && !this.el.contains(target)) {
      this.isExpanded = false
    }
  }

  private handleKeydownOnInput(event: KeyboardEvent) {
    switch (event.key) {
      case 'Tab':
        this.closeSelect()
        break
      case 'ArrowDown':
        event.preventDefault()
        if (!this.isExpanded) {
          this.openSelect()
        }
        this.focusItemInDirection('next')
        break
      case 'ArrowUp':
        event.preventDefault()
        if (!this.isExpanded) {
          this.openSelect()
        }
        this.focusItemInDirection('previous')
        break
      case 'Escape':
        this.closeSelect()
        this.resetFocus()
        break
      case 'Enter':
        this.toggleOpen()
        break
      default:
    }
  }

  private handleKeydownOnList(event: KeyboardEvent) {
    event.preventDefault()
    switch (event.key) {
      case 'Tab':
      case 'Escape':
        this.resetFocus()
        break
      case 'ArrowUp':
        this.focusItemInDirection('previous')
        break
      case 'ArrowDown':
        this.focusItemInDirection('next')
        break
      default:
        break
    }
  }

  private handleKeydownOnItem(event: KeyboardEvent, index: number) {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault()
      this.handleSelection(index) // Garante que o índice correto será usado
    } else if (event.key === 'Escape') {
      this.closeSelect()
    }
  }

  private handleSelectAllKeyDown(event: KeyboardEvent) {
    if (event.key === 'Enter' || event.key === ' ') {
      event.preventDefault()
      this.toggleSelectAll()
    } else if (event.key === 'Escape') {
      event.preventDefault()
      this.closeSelect()
    }
  }

  private resetFocus() {
    const input = this.el.shadowRoot.querySelector('br-input') as HTMLElement
    input.focus()
  }

  private focusItemInDirection(direction: 'next' | 'previous') {
    let itemCount = this.parsedOptions.length
    if (this.isMultiple) itemCount += 1

    this.focusedIndex =
      direction === 'next' ? (this.focusedIndex + 1) % itemCount : (this.focusedIndex - 1 + itemCount) % itemCount

    this.focusItem(this.focusedIndex)
  }

  private focusItem(index: number) {
    const items = this.el.shadowRoot.querySelectorAll('.br-item')
    const item = items[index] as HTMLElement
    item.focus()
  }

  private openSelect() {
    this.isExpanded = true
  }

  private closeSelect() {
    this.isExpanded = false
  }

  private handleSelection(index: number) {
    if (index < 0 || index >= this.parsedOptions.length) {
      return
    }

    if (this.isMultiple) {
      this.parsedOptions[index].selected = !this.parsedOptions[index].selected
    } else {
      this.parsedOptions.forEach((option) => (option.selected = false))
      this.parsedOptions[index].selected = true
      this.isExpanded = false
    }
    this.updateSelectedOptions()
    this.emitSelectedOptions()
    this.updateSelectedAllState()
  }

  private updateSelectedOptions() {
    this.inputValue = this.getStringValue(this.parsedOptions.filter((option) => option.selected))
  }

  private getStringValue(options) {
    if (options.length > 0) {
      const firstSelected = options[0].label || options[0].value
      if (this.isMultiple) {
        const howManySelected = options.length > 1 ? ` + (${options.length - 1})` : ''
        return `${firstSelected}${howManySelected}`
      } else {
        return firstSelected
      }
    }
    return ''
  }

  private emitSelectedOptions() {
    const selectedOptions = this.parsedOptions
      .filter((option) => option.selected === true)
      .map((option) => option.label ?? option.value)

    this.brDidSelectChange.emit(selectedOptions)
  }

  private updateSelectedAllState() {
    const allSelected = this.parsedOptions.every((option) => option.selected)
    this.isAllSelected = allSelected
  }

  private toggleSelectAll() {
    if (this.isAllSelected) {
      this.parsedOptions.forEach((option) => (option.selected = false))
      this.isAllSelected = false
    } else {
      this.parsedOptions.forEach((option) => (option.selected = true))
      this.isAllSelected = true
    }

    this.updateSelectedOptions()
    this.emitSelectedOptions()
  }

  private renderInput() {
    return (
      <br-input
        value={this.inputValue}
        label={this.label}
        placeholder={this.placeholder}
        readonly
        onClick={() => this.toggleOpen()}
        onKeyDown={(event) => this.handleKeydownOnInput(event)}
      >
        {this.showSearchIcon && <br-icon slot="icon" icon-name="fa-solid:search" aria-hidden="true"></br-icon>}
        <br-icon
          slot="action"
          icon-name={this.isExpanded ? 'fa-solid:angle-up' : 'fa-solid:angle-down'}
          height="16"
          aria-hidden="true"
        ></br-icon>
      </br-input>
    )
  }

  private renderSelectAllCheckbox() {
    return (
      <div
        class={`br-item highlighted ${this.isAllSelected ? 'selected' : ''}`}
        data-all="data-all"
        tabindex="-1"
        role="option"
        aria-selected={this.isAllSelected}
        onKeyDown={(event) => this.handleSelectAllKeyDown(event)}
      >
        <br-checkbox
          name={`checkbox-${this.customId}`}
          label={this.isAllSelected ? this.unselectAllLabel : this.selectAllLabel}
          checked={this.isAllSelected}
          onCheckedChange={() => this.toggleSelectAll()}
        ></br-checkbox>
      </div>
    )
  }

  private renderOptions() {
    return this.parsedOptions.map((option, index) => (
      <div
        class={`br-item ${option.selected ? 'selected' : ''}`}
        key={`${option.label}-${this.customId}`}
        role="option"
        tabindex={this.focusedIndex === index ? 0 : -1}
        aria-selected={option.selected}
        onClick={() => this.handleSelection(index)}
        onKeyDown={(event) => this.handleKeydownOnItem(event, index)}
      >
        {this.isMultiple ? (
          <br-checkbox
            name={`checkbox-${index}-${this.customId}`}
            label={option.label || option.value}
            checked={option.selected === true}
            onCheckedChange={() => this.handleSelection(index)}
          />
        ) : (
          <br-radio
            exportparts="radio-input, radio-label"
            name={`radio-${index}-${this.customId}`}
            label={option.label || option.value}
            checked={option.selected === true}
            onCheckedChange={() => this.handleSelection(index)}
          />
        )}
      </div>
    ))
  }

  /**
   * Determina um id para o radio.
   */
  private getComputedId() {
    return this.customId?.length > 0 ? this.customId : `br-select-${this.customId}`
  }

  /**
   * Inverte o valor da prop `isOpen`
   */
  @Method()
  async toggleOpen() {
    this.isExpanded = !this.isExpanded
  }

  private getCssClassMap(): CssClassMap {
    return {
      'br-select': true,
    }
  }

  render() {
    return (
      <div class={this.getCssClassMap()} id={this.getComputedId()}>
        {this.renderInput()}

        {this.isExpanded && (
          <br-list tabindex={-1} onKeyDown={(event) => this.handleKeydownOnList(event)}>
            {this.isMultiple && this.renderSelectAllCheckbox()}
            {this.renderOptions()}
          </br-list>
        )}
      </div>
    )
  }
}

let selectId = 0
