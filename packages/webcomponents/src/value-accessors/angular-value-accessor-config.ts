import { ValueAccessorConfig } from '@stencil/angular-output-target'

/**
 * Define os componentes que devem ser integrados com o ngModel (por exemplo, componentes de formulários).
 * Permite definir qual é a propriedade alvo (por exemplo, 'value') e qual evento causará a mudança da propriedade.
 */
export const angularValueAccessorBindings: ValueAccessorConfig[] = [
  {
    elementSelectors: ['br-input:not[type="number"]', 'br-textarea'],
    event: 'valueChange',
    targetAttr: 'value',
    type: 'text',
  },
  {
    elementSelectors: ['br-input[type="number"]'],
    event: 'valueChange',
    targetAttr: 'value',
    type: 'number',
  },
  // {
  //   elementSelectors: ['br-select'],
  //   event: 'brDidSelectChange',
  //   targetAttr: 'inputValue',
  //   type: 'select',
  // },
  {
    elementSelectors: ['br-checkbox', 'br-switch'],
    event: 'checkedChange',
    targetAttr: 'checked',
    type: 'boolean',
  },
  {
    elementSelectors: ['br-radio'],
    event: 'checkedChange',
    targetAttr: 'checked',
    type: 'radio',
  },
]
