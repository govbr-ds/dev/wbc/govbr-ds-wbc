
import * as d from './components';

export const DIRECTIVES = [
  d.BrAvatar,
  d.BrButton,
  d.BrCheckbox,
  d.BrCheckgroup,
  d.BrDropdown,
  d.BrIcon,
  d.BrInput,
  d.BrItem,
  d.BrList,
  d.BrLoading,
  d.BrMessage,
  d.BrRadio,
  d.BrSelect,
  d.BrSwitch,
  d.BrTag,
  d.BrTextarea,
  d.BrUpload
];
