/* tslint:disable */
/* auto-generated angular directive proxies */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, NgZone } from '@angular/core';

import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import { Components } from '@govbr-ds/webcomponents';


@ProxyCmp({
  inputs: ['alt', 'customId', 'density', 'disabled', 'iconHeight', 'iconMarginTop', 'iconWidth', 'isIconic', 'src', 'text']
})
@Component({
  selector: 'br-avatar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['alt', 'customId', 'density', 'disabled', 'iconHeight', 'iconMarginTop', 'iconWidth', 'isIconic', 'src', 'text'],
})
export class BrAvatar {
  protected el: HTMLBrAvatarElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrAvatar extends Components.BrAvatar {}


@ProxyCmp({
  inputs: ['colorMode', 'customId', 'density', 'disabled', 'emphasis', 'isActive', 'isLoading', 'shape', 'type', 'value']
})
@Component({
  selector: 'br-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['colorMode', 'customId', 'density', 'disabled', 'emphasis', 'isActive', 'isLoading', 'shape', 'type', 'value'],
})
export class BrButton {
  protected el: HTMLBrButtonElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrButton extends Components.BrButton {}


@ProxyCmp({
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'indeterminate', 'label', 'name', 'state', 'value'],
  methods: ['setIndeterminate', 'toggleChecked']
})
@Component({
  selector: 'br-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'indeterminate', 'label', 'name', 'state', 'value'],
})
export class BrCheckbox {
  protected el: HTMLBrCheckboxElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange', 'indeterminateChange']);
  }
}


export declare interface BrCheckbox extends Components.BrCheckbox {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
  /**
   * Disparado depois que o valor do `indeterminate` foi alterado.
   */
  indeterminateChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  inputs: ['customId', 'indeterminate', 'label', 'labelDesselecionado', 'labelSelecionado']
})
@Component({
  selector: 'br-checkgroup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'indeterminate', 'label', 'labelDesselecionado', 'labelSelecionado'],
})
export class BrCheckgroup {
  protected el: HTMLBrCheckgroupElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrCheckgroup extends Components.BrCheckgroup {}


@ProxyCmp({
  inputs: ['customId', 'isOpen'],
  methods: ['open', 'hide']
})
@Component({
  selector: 'br-dropdown',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isOpen'],
})
export class BrDropdown {
  protected el: HTMLBrDropdownElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDropdownChange']);
  }
}


export declare interface BrDropdown extends Components.BrDropdown {
  /**
   * Evento emitido quando o estado do dropdown muda.
   */
  brDropdownChange: EventEmitter<CustomEvent<{ 'is-opened': boolean }>>;
}


@ProxyCmp({
  inputs: ['cssClasses', 'customId', 'flip', 'height', 'iconName', 'isInline', 'rotate', 'width']
})
@Component({
  selector: 'br-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['cssClasses', 'customId', 'flip', 'height', 'iconName', 'isInline', 'rotate', 'width'],
})
export class BrIcon {
  protected el: HTMLBrIconElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrIcon extends Components.BrIcon {}


@ProxyCmp({
  inputs: ['autocomplete', 'autocorrect', 'customId', 'density', 'disabled', 'helpText', 'isHighlight', 'isInline', 'label', 'max', 'maxlength', 'min', 'minlength', 'multiple', 'name', 'pattern', 'placeholder', 'readonly', 'required', 'state', 'step', 'type', 'value']
})
@Component({
  selector: 'br-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['autocomplete', 'autocorrect', 'customId', 'density', 'disabled', 'helpText', 'isHighlight', 'isInline', 'label', 'max', 'maxlength', 'min', 'minlength', 'multiple', 'name', 'pattern', 'placeholder', 'readonly', 'required', 'state', 'step', 'type', 'value'],
})
export class BrInput {
  protected el: HTMLBrInputElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChange']);
  }
}


export declare interface BrInput extends Components.BrInput {
  /**
   * Valor atualizado do input
   */
  valueChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  inputs: ['customId', 'density', 'disabled', 'href', 'isActive', 'isButton', 'isInteractive', 'isSelected', 'target', 'type', 'value']
})
@Component({
  selector: 'br-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'density', 'disabled', 'href', 'isActive', 'isButton', 'isInteractive', 'isSelected', 'target', 'type', 'value'],
})
export class BrItem {
  protected el: HTMLBrItemElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidClick', 'brDidSelect']);
  }
}


export declare interface BrItem extends Components.BrItem {
  /**
   * Evento customizado emitido quando o item é clicado, aplicável apenas se o item for um botão (`<button>`).
Pode ser utilizado para ações personalizadas, exceto quando o item está desativado.
   */
  brDidClick: EventEmitter<CustomEvent<any>>;
  /**
   * Evento customizado aplicável para todos os tipos de elementos (`div`, `button`, `a`), emitido quando o item é selecionado e desde que a propriedade `isInteractive` esteja presente.
   */
  brDidSelect: EventEmitter<CustomEvent<{ selected: boolean }>>;
}


@ProxyCmp({
  inputs: ['customId', 'header', 'hideHeaderDivider', 'isHorizontal']
})
@Component({
  selector: 'br-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'header', 'hideHeaderDivider', 'isHorizontal'],
})
export class BrList {
  protected el: HTMLBrListElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrList extends Components.BrList {}


@ProxyCmp({
  inputs: ['customId', 'isMedium', 'isProgress', 'label', 'progressPercent']
})
@Component({
  selector: 'br-loading',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isMedium', 'isProgress', 'label', 'progressPercent'],
})
export class BrLoading {
  protected el: HTMLBrLoadingElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrLoading extends Components.BrLoading {}


@ProxyCmp({
  inputs: ['autoRemove', 'customId', 'isClosable', 'isFeedback', 'isInline', 'message', 'messageTitle', 'showIcon', 'state']
})
@Component({
  selector: 'br-message',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['autoRemove', 'customId', 'isClosable', 'isFeedback', 'isInline', 'message', 'messageTitle', 'showIcon', 'state'],
})
export class BrMessage {
  protected el: HTMLBrMessageElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidClose']);
  }
}


export declare interface BrMessage extends Components.BrMessage {
  /**
   * Evento emitido quando o usuário fecha a mensagem, se closable for true.
   */
  brDidClose: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'label', 'name', 'state', 'value'],
  methods: ['toggleChecked']
})
@Component({
  selector: 'br-radio',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'disabled', 'hasHiddenLabel', 'label', 'name', 'state', 'value'],
})
export class BrRadio {
  protected el: HTMLBrRadioElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange']);
  }
}


export declare interface BrRadio extends Components.BrRadio {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  inputs: ['customId', 'isMultiple', 'isOpen', 'label', 'options', 'placeholder', 'selectAllLabel', 'showSearchIcon', 'unselectAllLabel'],
  methods: ['toggleOpen']
})
@Component({
  selector: 'br-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'isMultiple', 'isOpen', 'label', 'options', 'placeholder', 'selectAllLabel', 'showSearchIcon', 'unselectAllLabel'],
})
export class BrSelect {
  protected el: HTMLBrSelectElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['brDidSelectChange']);
  }
}


export declare interface BrSelect extends Components.BrSelect {
  /**
   * Evento emitido sempre que houver atualização nos itens selecionados.
   */
  brDidSelectChange: EventEmitter<CustomEvent<any>>;
}


@ProxyCmp({
  inputs: ['checked', 'customId', 'density', 'disabled', 'hasIcon', 'label', 'labelOff', 'labelOn', 'labelPosition', 'name', 'value'],
  methods: ['toggleChecked']
})
@Component({
  selector: 'br-switch',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['checked', 'customId', 'density', 'disabled', 'hasIcon', 'label', 'labelOff', 'labelOn', 'labelPosition', 'name', 'value'],
})
export class BrSwitch {
  protected el: HTMLBrSwitchElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['checkedChange']);
  }
}


export declare interface BrSwitch extends Components.BrSwitch {
  /**
   * Disparado depois que o valor do `checked` foi alterado
   */
  checkedChange: EventEmitter<CustomEvent<boolean>>;
}


@ProxyCmp({
  inputs: ['color', 'customId', 'density', 'disabled', 'iconName', 'interaction', 'interactionSelect', 'label', 'multiple', 'name', 'selected', 'shape', 'status']
})
@Component({
  selector: 'br-tag',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['color', 'customId', 'density', 'disabled', 'iconName', 'interaction', 'interactionSelect', 'label', 'multiple', 'name', 'selected', 'shape', 'status'],
})
export class BrTag {
  protected el: HTMLBrTagElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['radioSelected']);
  }
}


export declare interface BrTag extends Components.BrTag {
  /**
   * Evento emitido quando a tag é selecionada.
   */
  radioSelected: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  inputs: ['customId', 'density', 'disabled', 'isInline', 'label', 'maxlength', 'placeholder', 'showCounter', 'state', 'value'],
  methods: ['setValue']
})
@Component({
  selector: 'br-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['customId', 'density', 'disabled', 'isInline', 'label', 'maxlength', 'placeholder', 'showCounter', 'state', 'value'],
})
export class BrTextarea {
  protected el: HTMLBrTextareaElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    proxyOutputs(this, this.el, ['valueChange']);
  }
}


export declare interface BrTextarea extends Components.BrTextarea {
  /**
   * Valor atualizado do textarea
   */
  valueChange: EventEmitter<CustomEvent<string>>;
}


@ProxyCmp({
  inputs: ['accept', 'disabled', 'label', 'multiple', 'state']
})
@Component({
  selector: 'br-upload',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['accept', 'disabled', 'label', 'multiple', 'state'],
})
export class BrUpload {
  protected el: HTMLBrUploadElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}


export declare interface BrUpload extends Components.BrUpload {}


