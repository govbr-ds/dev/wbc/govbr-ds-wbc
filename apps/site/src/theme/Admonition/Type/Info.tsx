import Translate from '@docusaurus/Translate'
import IconInfo from '@theme/Admonition/Icon/Info'
import AdmonitionLayout from '@theme/Admonition/Layout'
import type { Props } from '@theme/Admonition/Type/Info'
import clsx from 'clsx'

const infimaClassName = 'alert alert--info'

const defaultProps = {
  icon: <IconInfo />,
  title: (
    <Translate id="theme.admonition.info" description="The default label used for the Info admonition (:::info)">
      info
    </Translate>
  ),
}

export default function AdmonitionTypeInfo(props: Props): JSX.Element {
  return (
    <AdmonitionLayout {...defaultProps} {...props} className={clsx(infimaClassName, 'info')}>
      {props.children}
    </AdmonitionLayout>
  )
}
