import Translate from '@docusaurus/Translate'
import IconDanger from '@theme/Admonition/Icon/Danger'
import AdmonitionLayout from '@theme/Admonition/Layout'
import type { Props } from '@theme/Admonition/Type/Danger'
import clsx from 'clsx'

const infimaClassName = 'alert alert--danger'

const defaultProps = {
  icon: <IconDanger />,
  title: (
    <Translate id="theme.admonition.danger" description="The default label used for the Danger admonition (:::danger)">
      danger
    </Translate>
  ),
}

export default function AdmonitionTypeDanger(props: Props): JSX.Element {
  return (
    <AdmonitionLayout {...defaultProps} {...props} className={clsx(infimaClassName, 'danger')}>
      {props.children}
    </AdmonitionLayout>
  )
}
