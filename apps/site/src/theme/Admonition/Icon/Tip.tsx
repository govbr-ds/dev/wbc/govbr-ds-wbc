export default function AdmonitionIconTip(): JSX.Element {
  return (
    <div className="icon">
      <i className="fas fa-check-circle fa-lg" aria-hidden="true"></i>
    </div>
  )
}
