import clsx from 'clsx'

export default function FooterLayout({ style, links, logo, copyright }) {
  return (
    <footer
      className={clsx('br-footer', {
        'footer--dark': style === 'dark',
      })}
    >
      <div className="container-lg">
        {logo}
        {links}
      </div>
      <span className="br-divider my-3"></span>
      <div className="container-lg">{copyright}</div>
    </footer>
  )
}
