import LinkItem from '@theme/Footer/LinkItem'

function ColumnLinkItem({ item }) {
  return item.html ? (
    <li
      className="footer__item"
      // Developer provided the HTML, so assume it's safe.
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: item.html }}
    />
  ) : (
    <LinkItem item={item} />
  )
}
function Column({ column }) {
  return (
    <div className="col-2">
      <div className="br-item header">
        <div className="content text-down-01 text-bold text-uppercase">{column.title}</div>
        <div className="support">
          <i className="fas fa-angle-down" aria-hidden="true"></i>
        </div>
      </div>
      <div className="br-list">
        <span className="br-divider d-md-none"></span>
        {column.items.map((item, i) => (
          <ColumnLinkItem key={i} item={item} />
        ))}
        <span className="br-divider d-md-none"> </span>
      </div>
    </div>
  )
}
export default function FooterLinksMultiColumn({ columns }) {
  return (
    <div className="br-list horizontal">
      {columns.map((column, i) => (
        <Column key={i} column={column} />
      ))}
    </div>
  )
}
