import Link from '@docusaurus/Link'
import isInternalUrl from '@docusaurus/isInternalUrl'
import useBaseUrl from '@docusaurus/useBaseUrl'
import IconExternalLink from '@theme/Icon/ExternalLink'
export default function FooterLinkItem({ item }) {
  const { to, href, label, prependBaseUrlToHref, ...props } = item
  const toUrl = useBaseUrl(to)
  const normalizedHref = useBaseUrl(href, { forcePrependBaseUrl: true })
  return (
    <Link
      className="br-item"
      {...(href
        ? {
            href: prependBaseUrlToHref ? normalizedHref : href,
          }
        : {
            to: toUrl,
          })}
      {...props}
    >
      <div className="content">{label}</div>
      {href && !isInternalUrl(href) && <IconExternalLink />}
    </Link>
  )
}
