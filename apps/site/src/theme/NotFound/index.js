import { translate } from '@docusaurus/Translate'
import { PageMetadata } from '@docusaurus/theme-common'
import Layout from '@theme/Layout'
import NotFoundContent from '@theme/NotFound/Content'
export default function Index() {
  const title = translate({
    id: 'theme.NotFound.title',
    message: 'Página não encontrada',
  })
  return (
    <>
      <PageMetadata title={title} />
      <Layout>
        <NotFoundContent />
      </Layout>
    </>
  )
}
