import {
  SandpackCodeEditor,
  SandpackConsole,
  SandpackLayout,
  SandpackPreview,
  SandpackProvider,
  useSandpack,
} from '@codesandbox/sandpack-react'
import { useActiveDocContext } from '@docusaurus/plugin-content-docs/client'
import useBaseUrl from '@docusaurus/useBaseUrl'
import { useEffect, useState } from 'react'
import styles from './styles.module.css'

/**
 * Props para o componente SandpackSync.
 * @typedef {Object} SandpackSyncProps
 * @property {string} title - O título do projeto.
 * @property {string} wbcScriptSrc - A URL de origem para o script de Web Components.
 * @property {string} theme - O tema do Sandpack, pode ser 'light' ou 'dark'.
 */
interface SandpackSyncProps {
  title: string
  wbcScriptSrc: string
  theme: string
}

/**
 * @typedef {Object} SandpackProps
 * @property {'light' | 'dark'} [theme] - O tema do Sandpack.
 * @property {boolean} [showConsole] - Flag para mostrar o console.
 * @property {boolean} [showEditor] - Flag para mostrar o editor de código.
 * @property {boolean} [showPreview] - Flag para mostrar a área de pré-visualização.
 * @property {string} [codeHeight] - Altura do bloco de código.
 * @property {string} [consoleHeight] - Altura do console.
 * @property {string} [coreVersion] - A versão da biblioteca core.
 * @property {string} [cssCode] - O código CSS a ser incluído no projeto.
 * @property {string} [htmlCode] - O código HTML a ser incluído no projeto.
 * @property {string} [jsCode] - O código JavaScript a ser incluído no projeto.
 * @property {string} [previewHeight] - Altura do preview.
 * @property {string} [scssCode] - O código SCSS a ser incluído no projeto.
 * @property {string} [title] - O título do projeto.
 */
interface SandpackProps {
  codeHeight?: string
  consoleHeight?: string
  coreVersion?: string
  cssCode?: string
  htmlCode?: string
  jsCode?: string
  previewHeight?: string
  scssCode?: string
  showConsole?: boolean
  showEditor?: boolean
  showPreview?: boolean
  showTabs?: boolean
  theme?: 'light' | 'dark'
  title?: string
}

/**
 * Componente para sincronizar arquivos do Sandpack com o conteúdo HTML.
 *
 * @param {SandpackSyncProps} props - As propriedades para o componente SandpackSync.
 * @returns {null} Retorna null.
 */
const SandpackSync = ({ title, wbcScriptSrc, theme = 'light' }: SandpackSyncProps) => {
  const { sandpack } = useSandpack()
  const [htmlContent, setHtmlContent] = useState(sandpack.files['/example.html']?.code || '')

  useEffect(() => {
    const contentHtml = sandpack.files['/example.html']?.code || ''

    // Sincroniza o conteúdo HTML, atualizando o arquivo index.html se houver mudanças
    if (contentHtml !== htmlContent) {
      setHtmlContent(contentHtml)

      if (contentHtml) {
        sandpack.updateFile(
          '/index.html',
          /*html*/ `
<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5.0" />
    <title>${title}</title>
  </head>
<body class="${theme === 'dark' ? 'bg-blue-warm-vivid-90' : ''}">
    ${contentHtml}
    <script type="module" src="${wbcScriptSrc}"></script>
    <script src="./example.js"></script>
  </body>
</html>`
        )
      }
    }
  })

  return null
}

/**
 * Componente principal do Sandpack para renderizar o editor de código, console e pré-visualização.
 *
 * @param {SandpackProps} props - As propriedades para o componente Sandpack.
 * @returns {JSX.Element} O componente Sandpack renderizado.
 */
export default ({
  codeHeight = '200px',
  consoleHeight = '150px',
  coreVersion = '3',
  cssCode,
  htmlCode,
  jsCode,
  previewHeight = '200px',
  scssCode,
  showConsole = true,
  showEditor = true,
  showPreview = true,
  showTabs = false,
  theme = 'light',
  title,
}: SandpackProps) => {
  /**
   * Função para abrir uma nova aba e abrir uma issue no GitLab.
   */
  const handleOpenIssue = () => {
    const issueTitle = encodeURIComponent(`Feedback - ${title}`)
    const gitlabIssueUrl = `https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new?issue[title]=${issueTitle}`
    window.open(gitlabIssueUrl, '_blank')
  }

  const docsPluginId = undefined
  // Obtém a versão ativa do Web Components a partir do contexto de documentação
  const wbcVersion = useActiveDocContext(docsPluginId).activeVersion.label.toLowerCase()

  // Define a URL do script de Web Components dependendo do ambiente (produção ou desenvolvimento)
  const timestamp = new Date().getTime()
  const wbcScriptSrc =
    process.env.NODE_ENV !== 'production'
      ? `http://localhost:${window.location.port}${useBaseUrl('/')}dist/webcomponents/webcomponents.esm.js?t=${timestamp}`
      : `https://cdn.jsdelivr.net/npm/@govbr-ds/webcomponents@${wbcVersion}/dist/webcomponents/webcomponents.esm.js?t=${timestamp}`

  const files: { [key: string]: { code: string; hidden?: boolean; active?: boolean; readOnly?: boolean } } = {}

  // Define o arquivo HTML se o código HTML foi fornecido
  if (htmlCode) {
    files['/index.html'] = {
      code: /*html*/ `<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5.0" />
    ${title ? `<title>${title}</title>` : ''}
  </head>
  <body class="${theme === 'dark' ? 'bg-blue-warm-vivid-90' : ''}">
    ${htmlCode}
    <script type="module" src="${wbcScriptSrc}"></script>
    <script src="./example.js"></script>
  </body>
</html>`,
      hidden: true,
    }
    files['/example.html'] = { code: htmlCode, active: true }
  }

  // REFACTOR: Hoje não temos necessidade desses arquivos. Vou deixar eles escondidos do usuário esperando a avaliação pelos designers
  // Define o arquivo JS se o código JS foi fornecido
  if (jsCode) files['/example.js'] = { code: jsCode, hidden: true, readOnly: true }

  // Define o arquivo SCSS se o código SCSS foi fornecido
  if (scssCode) files['/styles.scss'] = { code: scssCode, hidden: true, readOnly: true }

  // Define o arquivo CSS se o código CSS foi fornecido
  if (cssCode) files['/styles.css'] = { code: cssCode, hidden: true, readOnly: true }

  return (
    <>
      {title && <h3>{title}</h3>}
      <SandpackProvider
        template="static"
        files={files}
        options={{
          recompileDelay: 1000,
          recompileMode: 'delayed',
          externalResources: [
            'https://cdngovbr-ds.estaleiro.serpro.gov.br/design-system/fonts/rawline/css/rawline.css',
            'https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900&amp;display=swap',
            'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css',
            `https://cdn.jsdelivr.net/npm/@govbr-ds/core@${coreVersion}/dist/core.min.css`,
          ],
          classes: {
            'sp-wrapper': 'custom-wrapper',
            'sp-layout': styles.layout,
            'sp-tab-button': 'custom-tab',
          },
        }}
      >
        <SandpackSync title={title} wbcScriptSrc={wbcScriptSrc} theme={theme} />

        <SandpackLayout style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ display: 'flex', flexDirection: 'column', flex: 1 }}>
            {showPreview && (
              <SandpackPreview
                showOpenInCodeSandbox={false}
                showRefreshButton={false}
                showOpenNewtab={false}
                actionsChildren={
                  <br-button
                    id="feedback-btn"
                    shape="circle"
                    aria-label="Criar issue no Gitlab.com"
                    onClick={handleOpenIssue}
                  >
                    <i className="fab fa-gitlab"></i>
                  </br-button>
                }
                style={{ height: `${previewHeight}` }}
              />
            )}
            {showEditor && (
              <SandpackCodeEditor
                showTabs={showTabs}
                showLineNumbers={true}
                showInlineErrors
                wrapContent
                closableTabs
                style={{ height: `${codeHeight}` }}
                initMode="lazy"
              />
            )}
          </div>

          {showConsole && (
            <div style={{ flexShrink: 0 }}>
              <SandpackConsole style={{ height: `${consoleHeight}` }} />
            </div>
          )}
        </SandpackLayout>
      </SandpackProvider>
    </>
  )
}
