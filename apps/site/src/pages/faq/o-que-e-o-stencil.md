Stencil é um framework para construção de componentes Web Components. O Stencil oferece diversos recursos para facilitar o desenvolvimento de bibliotecas de componentes:

**Compilador**: O compilador Stencil converte componentes TypeScript em JavaScript otimizado para produção.
**Shadow DOM**: O Stencil usa Shadow DOM para encapsular o estilo e o comportamento dos componentes, evitando conflitos com outros elementos na página.
**HMR (Hot Module Reload)**: O Stencil permite recarregar os componentes em tempo real, facilitando o desenvolvimento e a depuração.
**Roteamento**: O Stencil oferece suporte a roteamento para criar aplicações web de página única.

Stencil é um compilador de Web Components de código aberto, desenvolvido pela equipe do Ionic Framework. Ele permite criar componentes web reutilizáveis utilizando TypeScript, JSX e outras tecnologias modernas.

## Características

- **Desempenho otimizado:** Stencil gera código altamente otimizado, resultando em componentes com excelente performance.
- **Suporte a TypeScript:** Stencil é totalmente compatível com TypeScript, permitindo aos desenvolvedores utilizar tipos estáticos para garantir a integridade do código.
- **Renderização Universal:** Stencil suporta renderização tanto do lado do cliente quanto do lado do servidor, facilitando a criação de aplicações universais.
- **Componentes:** Stencil promove a reutilização de código ao permitir a criação de componentes web encapsulados e independentes.

## Referências

- [Stencil](https://stenciljs.com)
