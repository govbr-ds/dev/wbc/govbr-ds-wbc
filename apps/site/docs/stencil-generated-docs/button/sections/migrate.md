## Guia de Migração de Web Components de 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

#### Propriedades retiradas na versão 2.x

| Propriedade | Descrição                                    | Tipo      | Padrão  |
| ----------- | -------------------------------------------- | --------- | ------- |
| `block`     | Usado para dar o formato de bloco ao botão   | `boolean` | `false` |
| `circle`    | Usado para dar o formato de círculo ao botão | `boolean` | `false` |
| `type`      | Usado para definir a ênfase do botão         | `string`  | `null`  |

#### Propriedades incluídas na versão 2.x

| Propriedade | Descrição                  | Tipo                               | Padrão      |
| ----------- | -------------------------- | ---------------------------------- | ----------- |
| `emphasis`  | Informa a ênfase do botão  | `primary \| secondary \| tertiary` | `tertiary`  |
| `shape`     | Informa o formato do botão | `'circle \| block \| pill`         | `pill`      |
| `type`      | Informa o tipo do botão    | `button \| reset \| submit`        | `undefined` |

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).