## Eventos

| Evento        | Descrição                    | Tipo                  |
| ------------- | ---------------------------- | --------------------- |
| `valueChange` | Valor atualizado do textarea | `CustomEvent<string>` |