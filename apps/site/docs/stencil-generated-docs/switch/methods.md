## Métodos

### `toggleChecked() => Promise<void>`

Inverte o valor da prop `checked`

#### Retorna

Tipo: `Promise<void>`