## Guia de Migração de Web Components da 1.x (em Vue) para 2.x (em Stencil)

### Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue | Propriedade Stencil | Descrição                                                 | Tipo                             | Padrão       |
| --------------- | ------------------- | --------------------------------------------------------- | -------------------------------- | ------------ |
| `density`       | `density`           | Define os níveis de densidade para o elemento avatar      | `'small' \| 'medium' \| 'large'` | `'medium'`   |
| `iconic`        | `iconic`            | Define que o avatar apresentado será do tipo ícone        | `boolean`                        | `false`      |
| `image`         | `image`             | Define que o avatar apresentado será do tipo fotográfico  | `string`                         | `undefined`  |
| `name`          | `name`              | Nome do usuário que será apresentado no avatar pela letra | `string`                         | `'John Doe'` |

### Eventos

Entenda melhor como utilizar os eventos para o componente `<br-avatar></br-avatar>` no Vue e em StencilJS:

#### Vue

- Evento `click`: Evento emitido quando o avatar é clicado.

Exemplo:

```html
<br-avatar id="avatar-exemplo" name="John Doe"></br-avatar>
```

Adicionando o listener de evento:

```javascript
document.getElementById('avatar-exemplo').addEventListener('click', (e) => {
  console.log('Avatar clicado')
})
```

#### StencilJS

- Usando eventos em JSX: Automaticamente inclui o prefixo "on". Por exemplo, se o evento emitido for chamado `avatarClick`, a propriedade será chamada `onAvatarClick`.

```html
<!-- StencilJS -->
<br-avatar onClick="handleAvatarClick"></br-avatar>
```

- Ouvindo eventos de um elemento não JSX:

```html
<template>
  <br-avatar></br-avatar>
</template>

<script>
  const avatarElement = document.querySelector('br-avatar')
  avatarElement.addEventListener('click', (event) => {
    /* seu listener */
  })
</script>
```

| Evento                    | Descrição                                                                                                 |
| ------------------------- | --------------------------------------------------------------------------------------------------------- |
| `brComponentDidLoad`      | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidRender`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentDidUpdate`    | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentShouldUpdate` | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillLoad`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillRender`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brComponentWillUpdate`   | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brConnectedCallback`     | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |
| `brDisconnectedCallback`  | Consulte a documentação do [StencilJS](https://stenciljs.com/docs/component-lifecycle#connectedcallback). |

## Métodos

- `renderImageOrIcon()`: Método auxiliar que decide se deve renderizar uma imagem, um ícone ou a inicial do nome.

```javascript
<script>const avatarElement = document.querySelector('br-avatar'); avatarElement.renderImageOrIcon();</script>
```

### Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

### Exemplo Completo

Exemplo completo de como usar um componente Avatar em uma aplicação Web.

```html
<template>
  <br-avatar
    id="avatar-01"
    name="Jane Doe"
    image="path/to/image.jpg"
    density="large"
    onClick="handleAvatarClick"
  ></br-avatar>
</template>
<script>
  export default {
    methods: {
      handleAvatarClick(event) {
        console.log('Avatar clicado: ', event)
      },
    },
  }
</script>
```

### Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).