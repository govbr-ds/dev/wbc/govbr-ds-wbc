## Dependências

### Depende de

- [br-icon](../icon)

### Gráfico
```mermaid
graph TD;
  br-avatar --Depende---> br-icon
  click br-icon "src/components/icon" "Link para a documentação do componente icon"
  class br-icon depComponent
  class br-avatar mainComponent
```