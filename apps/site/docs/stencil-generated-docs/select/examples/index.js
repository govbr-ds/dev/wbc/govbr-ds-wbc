if (!window.elementsToListen && !window.eventsToListen) {
  const elementsToListen = document.querySelectorAll('br-select')

  const eventsToListen = ['click']

  elementsToListen.forEach((domElement) => {
    eventsToListen.forEach((event) => {
      domElement.addEventListener(event, function (event) {
        console.log({
          Evento: event.type,
          Elemento: domElement.outerHTML,
          ...(event.detail && { Detail: event.detail }),
          ...(event.data && { Data: event.data }),
        })
      })
    })
  })
}