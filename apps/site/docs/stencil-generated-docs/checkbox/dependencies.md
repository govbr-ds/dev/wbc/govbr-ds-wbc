## Dependências

### Usado por

 - [br-checkgroup](../checkgroup)
 - [br-select](../select)

### Gráfico
```mermaid
graph TD;
  br-checkgroup --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  br-select --Depende---> br-checkbox
  click br-checkbox "src/components/checkbox" "Link para a documentação do componente checkbox"
  class br-checkbox depComponent
  class br-checkbox mainComponent
```