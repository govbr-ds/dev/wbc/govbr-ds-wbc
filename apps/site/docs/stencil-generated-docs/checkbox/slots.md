## Slots

| Slot        | Descrição                                                                |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | O texto descritivo pode ser passado por propriedade (label) ou por slot. |