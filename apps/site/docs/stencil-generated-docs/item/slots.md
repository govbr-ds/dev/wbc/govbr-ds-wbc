## Slots

| Slot        | Descrição                                                                                                                                                              |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `"default"` | Área de conteúdo, podendo conter qualquer componente, exceto botões primários e componentes relacionados à navegação (como carrosséis, paginações, abas, menus, etc.). |
| `"end"`     | Área de recursos complementares, podendo conter componentes interativos, metadados e informações adicionais.                                                           |
| `"start"`   | Área de recursos visuais, podendo conter elementos como ícones, avatares e mídias.                                                                                     |