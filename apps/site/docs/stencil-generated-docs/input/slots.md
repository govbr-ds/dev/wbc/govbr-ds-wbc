## Slots

| Slot          | Descrição                                                                                                                                      |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| `"action"`    | Botão à direita do input.                                                                                                                      |
| `"feedback"`  | Mensagem de feedback como resposta específica a uma interação do usuário com o input. Pode ser feedback de erro, aviso, sucesso ou informação. |
| `"help-text"` | Personalização do texto de ajuda.                                                                                                              |
| `"icon"`      | Ícone à esquerda do input.                                                                                                                     |