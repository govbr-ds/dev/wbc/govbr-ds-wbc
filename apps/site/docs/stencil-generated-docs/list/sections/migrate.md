# List - Guia de Migração de Web Components de Vue para StencilJS

## Nomes de Propriedades

Alguns nomes de propriedades foram alterados, conforme abaixo:

| Propriedade Vue    | Propriedade Stencil | Descrição                                                                       | Tipo      | Padrão      |
| ------------------ | ------------------- | ------------------------------------------------------------------------------- | --------- | ----------- |
| `dataToggle`       | ---                 | Ativa a lista expansiva, onde os `br-item` filhos abrem/fecham ao clicá-los.    | `boolean` | `false`     |
| `dataUnique`       | ---                 | Ativa a abertura de um `br-item` da lista por vez quando a mesma é data-toggle. | `boolean` | `false`     |
| `density`          | ---                 | Níveis de densidade agora são definidos no componente `br-item`.                | `string`  | `undefined` |
| `hideTitleDivider` | `hideHeaderDivider` | Oculta o "divider" abaixo do header da lista (título e botões de ação).         | `boolean` | `false`     |
| `horizontal`       | `isHorizontal`      | Indica se a lista será horizontal. Por padrão, a lista é vertical.              | `boolean` | `false`     |
| `spaceBetween`     | ---                 | Indica que os itens da lista terão espaçamento uniforme entre eles.             | `boolean` | `false`     |
| `title`            | `listTitle`         | Define o título para a lista.                                                   | `string`  | `undefined` |

## Estilos

Se você estiver usando estilos personalizados, é importante notar que nossos componentes em Stencil utilizam Shadow DOM por padrão. Isso garante que os estilos internos dos componentes sejam isolados e não sejam afetados pelos estilos da aplicação que os utiliza, proporcionando um comportamento consistente e previsível.

## Considerações Finais

Caso encontre algum problema durante a migração ou surjam dúvidas, entre em contato com a equipe do [Padrão Digital de Governo](https://discord.gg/NkaVZERAT7).