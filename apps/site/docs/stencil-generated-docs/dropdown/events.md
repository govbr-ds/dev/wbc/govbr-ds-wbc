## Eventos

| Evento             | Descrição                                        | Tipo                                     |
| ------------------ | ------------------------------------------------ | ---------------------------------------- |
| `brDropdownChange` | Evento emitido quando o estado do dropdown muda. | `CustomEvent<{ 'is-opened': boolean; }>` |