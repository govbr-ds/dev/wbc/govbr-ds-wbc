## Slots

| Slot        | Descrição                                                                |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | Slot para adicionar os checkboxes que serão controlados pelo checkgroup. |