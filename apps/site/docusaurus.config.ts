import type { Options as ClientRedirectsOptions } from '@docusaurus/plugin-client-redirects'
import type { Options as BlogOptions } from '@docusaurus/plugin-content-blog'
import type { Options as DocsOptions } from '@docusaurus/plugin-content-docs'
import type { Options as PageOptions } from '@docusaurus/plugin-content-pages'
import type { Options as IdealImageOptions } from '@docusaurus/plugin-ideal-image'
import type * as Preset from '@docusaurus/preset-classic'
import npm2yarn from '@docusaurus/remark-plugin-npm2yarn'
import type { Config } from '@docusaurus/types'
import { themes as prismThemes } from 'prism-react-renderer'
import versions from './versions.json'
// import VersionsArchived from './versionsArchived.json'

// const ArchivedVersionsDropdownItems = Object.entries(VersionsArchived).splice(0, 5)

const config: Config = {
  title: 'Web Components - Padrão Digital de Governo',
  tagline: 'One library to rule them all',
  favicon: 'img/wc.svg',
  organizationName: 'SERPRO',
  projectName: 'Web Components GovBR-DS Site',
  url: 'https://govbr-ds.gitlab.io',
  baseUrl: '/bibliotecas/wbc/govbr-ds-wbc',
  baseUrlIssueBanner: true,
  trailingSlash: true,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  onBrokenAnchors: 'throw',

  // scripts: [
  //   {
  //     src: 'https://cdn.jsdelivr.net/npm/@govbr-ds/webcomponents@next/dist/webcomponents/webcomponents.esm.js',
  //     async: true,
  //     type: 'module',
  //   },
  // ],

  i18n: {
    defaultLocale: 'pt-BR',
    locales: ['pt-BR'],
    localeConfigs: {
      'pt-BR': {
        htmlLang: 'pt-BR',
      },
    },
  },

  customFields: {
    discordInvite: 'https://discord.gg/U5GwPfqhUP',
    repo: 'https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/',
  },

  staticDirectories: [
    'static',
    ...(process.env.NODE_ENV === 'development' ? ['../../dist/webcomponents'] : []),
    'docs/stencil-generated-docs/',
  ],

  markdown: {
    mermaid: true,
  },

  presets: [
    [
      'classic',
      {
        docs: {
          path: 'docs',
          showLastUpdateTime: true,
          sidebarPath: './sidebars.ts',
          exclude: ['**/stencil-generated-docs/**'],
          editUrl: 'https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/tree/main/apps/site',

          // https://docusaurus.io/docs/versioning
          lastVersion: getLastVersion(),
          // versions: {
          //   current: {
          //     label: 'Next',
          //     path: 'next',
          //   },
          // },
          remarkPlugins: [
            [npm2yarn, { sync: true }],
            //  remarkMath,
            //  configTabs
          ],
          // rehypePlugins: [rehypeKatex],
        } satisfies DocsOptions,
        blog: {
          showReadingTime: true,
          showLastUpdateTime: true,
          blogTitle: 'Notícias',
          blogDescription: 'Notícias sobre a biblioteca de web components do Padrão Digital de Governo',
          blogSidebarTitle: 'Notícias',
          blogSidebarCount: 'ALL',
          remarkPlugins: [npm2yarn],
          // feedOptions: {
          //   type: 'all',
          //   copyright: `Copyright © ${new Date().getFullYear()} Facebook, Inc.`,
          // },
          editUrl: 'https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/tree/main/apps/site',
        } satisfies BlogOptions,
        pages: {
          remarkPlugins: [npm2yarn],
        } satisfies PageOptions,
        // gtag: {
        //     trackingID: [''],
        //   }
        // sitemap: {
        //   ignorePatterns: ['{blog,pages}/**'],
        //   lastmod: 'date',
        //   priority: null,
        //   changefreq: null,
        // },
        theme: {
          customCss: './src/css/custom.css',
        },
      } satisfies Preset.Options,
    ],
  ],

  plugins: [
    [
      'client-redirects',
      {
        redirects: [
          // {
          //   from: [''],
          //   to: '',
          // },
        ],
      } satisfies ClientRedirectsOptions,
    ],
    [
      'ideal-image',
      {
        quality: 70,
        max: 1030,
        min: 640,
        steps: 2,
        disableInDev: false,
      } satisfies IdealImageOptions,
    ],
    '@docusaurus/theme-mermaid',
  ],

  themeConfig: {
    colorMode: {
      defaultMode: 'light',
      disableSwitch: true,
      respectPrefersColorScheme: false,
    },

    liveCodeBlock: {
      playgroundPosition: 'bottom',
    },

    docs: {
      sidebar: {
        hideable: true,
        autoCollapseCategories: false,
      },
    },

    // announcementBar: {
    //   id: 'announcement',
    //   content: `Esse site ainda está em fase de testes.</strong> Por favor crie uma issues caso encontre algum bug.`,
    //   backgroundColor: '#335286',
    //   textColor: '#fff',
    //   isCloseable: false,
    // },

    image: 'img/wc.svg',

    navbar: {
      // hideOnScroll: true,
      title: 'Web Components - GovBR-DS',
      logo: {
        alt: 'Logo de Web Components',
        src: 'img/wc.svg',
        // srcDark: 'img/docusaurus_keytar.svg',
        // width: 32,
        // height: 32,
      },
      items: [
        { to: '/blog', label: 'Notícias', position: 'left' },
        { to: '/docs', label: 'Documentação', position: 'left', docId: 'introducao', type: 'doc' },
        {
          to: '/faq',
          position: 'left',
          label: 'FAQ',
        },
        {
          to: '/versoes',
          position: 'left',
          label: 'Versões',
        },
        {
          type: 'search',
          position: 'right',
        },
        {
          type: 'docsVersionDropdown',
          position: 'right',
          dropdownActiveClassDisabled: true,
          dropdownItemsAfter: [
            // {
            //   type: 'html',
            //   value: '<hr class="dropdown-separator">',
            // },
            // {
            //   type: 'html',
            //   className: 'dropdown-archived-versions',
            //   value: '<b>Versões arquivadas</b>',
            // },
            // ...ArchivedVersionsDropdownItems.map(
            //   ([versionName, versionUrl]) => ({
            //     label: versionName,
            //     href: versionUrl,
            //   }),
            // ),
            // {
            //   href: '',
            //   label: '',
            // },
            {
              type: 'html',
              value: '<hr class="dropdown-separator">',
            },
            {
              to: '/versoes',
              label: 'Todas as versões',
            },
          ],
        },
        {
          href: 'https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/',
          html: "Repositório Gitlab <i class='fab fa-gitlab'></i>",
          position: 'right',
        },
        {
          href: 'https://www.gov.br/ds',
          position: 'right',
          label: 'GovBR-DS',
        },
        {
          href: 'https://discord.gg/U5GwPfqhUP',
          position: 'right',
          html: 'Comunidade no Discord <i class="fab fa-discord"></i>',
        },
        {
          href: 'https://gov.br/ds/wiki/',
          position: 'right',
          label: 'Wiki',
        },
      ],
    },

    footer: {
      style: 'dark',
      links: [
        {
          title: 'Comunidade',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/U5GwPfqhUP',
            },
          ],
        },
        {
          title: 'Código-fonte',
          items: [
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/',
            },
          ],
        },
      ],
      logo: {
        alt: 'Serpro logo',
        src: 'img/serpro/serpro-marca-negativo.svg',
        href: 'https://serpro.gov.br',
        width: 160,
        height: 51,
      },
      copyright: `O Padrão Digital de Governo utiliza as licenças CC0 1.0 Universal e MIT.`,
    },

    prism: {
      theme: prismThemes.github,
      darkTheme: prismThemes.dracula,
      additionalLanguages: ['jsx', 'tsx', 'bash', 'typescript', 'javascript', 'markup', 'css', 'json', 'diff'],
    },

    algolia: {
      appId: 'QJC46XU1HP',
      apiKey: 'd9d4295bd0cf6124e8a1bc3db9e5c5fc',
      indexName: 'gov-wc-site',
      contextualSearch: true,
    },
  } satisfies Preset.ThemeConfig,
}

function isPrerelease(version: string) {
  return version.includes('alpha') || version.includes('beta') || version.includes('rc')
}

function getLastVersion() {
  const firstStableVersion = versions.find((version) => !isPrerelease(version))
  return firstStableVersion ?? versions[0]
}

export default config
