# https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#ruby-condition
# https://www.rubydoc.info/gems/gitlab-triage
resource_rules:
  epics:
    rules:
      - name: Épicos sem labels
        conditions:
          author_username: govbr-ds
          state: opened
          labels:
            - None
        actions:
          comment_internal: true
          comment: |
            {{author}} Este épico está sem labels! Por favor, adicione labels apropriadas.
      - name: Épicos sem labels de papéis
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_role_labels?
        actions:
          comment_internal: true
          comment: |
            {{author}} Este épico deve ter pelo menos uma label relacionada aos papéis envolvidos: ['design', 'designops', 'dev', 'devops', 'gestão']!
      - name: Épicos sem labels de prioridade
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_priority_label?
        actions:
          comment_internal: true
          comment: |
            {{author}} Este épico deve ter uma label de `prioridade::`.
      - name: Épicos sem labels de categoria
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_category_label?
        actions:
          comment_internal: true
          comment: |
            {{author}} Este épico deve ter uma label de `categoria::`.
      - name: Épicos sem labels de tipo
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_tipo_label?
        actions:
          comment_internal: true
          comment: |
            {{author}} Este épico deve ter uma label de `tipo::`.
      - name: Fechar épico se todos os issues e épicos filhos estiverem fechados
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            all_children_closed?
        actions:
          status: close
          comment: |
            Épico fechado automaticamente, todos os épicos filhos e issues já estavam fechados.
          summarize:
            destination: govbr-ds/bibliotecas/wbc/govbr-ds-wbc
            title: "Resumo dos épicos fechados em #{Date.today.strftime('%d/%m/%Y')}"
            item: |
              - [ ] [{{title}}]({{web_url}})
            summary: |
              Esses épicos foram fechados automaticamente porque todos os seus issues e épicos filhos já estavam fechados.

              {{items}}

              Caso algum não possa ser fechado, por favor reabra e crie novas issues.

              /confidential
  issues:
    rules:
      - name: Issues sem labels (autor não é membro)
        conditions:
          author_username: govbr-ds
          labels:
            - None
          state: opened
          author_member:
            source: group
            condition: not_member_of
            source_id: 13519293
        actions:
          comment_internal: true
          comment: |
            Esta issue está aberta sem labels. Por favor, adicione labels apropriadas.
          mention:
            - matheus.monteiro
            - paula.coelho
            - natanael.leite
            - oliveira.rafael
      - name: Issues sem peso
        conditions:
          author_username: govbr-ds
          weight: None
          state: opened
          assignee_member:
            source: group
            condition: member_of
            source_id: 13519293
        actions:
          comment_internal: true
          comment: |
            {{assignees}} Esta issue está sem o peso. Por favor, adicione um peso apropriado.
      - name: Issues sem milestone
        conditions:
          author_username: govbr-ds
          milestone: None
          iteration: any
          state: opened
          assignee_member:
            source: group
            condition: member_of
            source_id: 13519293
        actions:
          comment_internal: true
          comment: |
            {{assignees}} Esta issue está sem o milestone. Por favor, adicione um milestone apropriado.
      - name: Issues sem iteração
        conditions:
          author_username: govbr-ds
          milestone: any
          iteration: None
          state: opened
          assignee_member:
            source: group
            condition: member_of
            source_id: 13519293
        actions:
          comment_internal: true
          comment: |
            {{assignees}} Esta issue está sem a iteração. Por favor, adicione uma iteração apropriada.
      - name: Issues em iteração vencida
        conditions:
          author_username: govbr-ds
          state: opened
          iteration: any
          ruby: |
            iteration = iteration_expired?
            iteration && iteration[:expired]
        actions:
          comment_internal: true
          comment: |
            {{assignees}} Esta issue está em uma iteração que já venceu. Avalie se é necessário passar ela para a próxima iteração ou cancelar a issue.
      - name: Issues da comunidade interação do time
        conditions:
          author_username: govbr-ds
          state: opened
          author_member:
            source: group
            condition: not_member_of
            source_id: 13519293
          ruby: |
            !discussion_from_group_member?
        actions:
          summarize:
            destination: govbr-ds/bibliotecas/wbc/govbr-ds-wbc
            title: "Issues da comunidade sem interação do time - #{Date.today.strftime('%d/%m/%Y')}"
            item: |
              - [ ] [{{title}}]({{web_url}})
            summary: |
              Essas issues foram abertas pela comunidade, mas estão sem interação há mais de 3 dias.

              {{items}}

              @matheus.monteiro @paula.coelho @natanael.leite @oliveira.rafael

              /confidential
      - name: Alerta sobre fluxos de issues
        conditions:
          author_username: govbr-ds
          state: opened
          iteration: any
          ruby: |
            !has_any_fluxo_label? && !has_suspended_label?
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue está sem labels de fluxo. O trabalho não começou?'
      - name: Issues sem labels de papéis
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_role_labels?
        actions:
          comment_internal: true
          comment: "{{author}} Esta issue deve ter pelo menos uma label relacionada aos papéis envolvidos: ['design', 'designops', 'dev', 'devops', 'gestão']!"
      # - name: Issues sem labels de prioridade
      #   conditions:
      #     author_username: govbr-ds
      #     state: opened
      #     ruby: |
      #       !has_priority_label?
      #   actions:
      #     comment_internal: true
      #     comment: "{{author}} Esta issue deve ter uma label de `prioridade::`."
      - name: Issues sem labels de categoria
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_category_label?
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue deve ter uma label de `categoria::`.'
      - name: Issues sem labels de tipo
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_tipo_label?
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue deve ter uma label de `tipo::`.'
      - name: Issues sem atualização a mais de 3 dias
        conditions:
          author_username: govbr-ds
          date:
            attribute: updated_at
            condition: older_than
            interval_type: days
            interval: 3
          state: opened
          iteration: any
          ruby: |
            !has_pending_label? || !has_pending_label? || !has_impacted_label?
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue está sem atualização há mais de 3 dias. Lembre-se de atualizar os dados (descrição, comentários, labels, etc.) sempre que possível.'
      - name: Issues relacionadas a MR fechado
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            related_merge_requests.any? && !related_merge_requests.any? { |mr| mr.state == 'opened' } && !existing_comment?("{{assignees}} Esta issue está relacionada a um MR fechado. Verifique se ela também deve ser fechada.")
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue está relacionada a um MR fechado. Verifique se ela também deve ser fechada.'
          summarize:
            destination: govbr-ds/bibliotecas/wbc/govbr-ds-wbc
            title: "Issues abertas, mas relacionadas com MRs fechados - #{Date.today.strftime('%d/%m/%Y')}"
            item: |
              - [ ] [{{title}}]({{web_url}})
            summary: |
              Essas issues estão abertas, mas tem relacionamento com MRs já fechados.

              {{items}}

              @matheus.monteiro @paula.coelho @natanael.leite @oliveira.rafael

              /confidential
      - name: Issues com MR aberto, mas sem a label de fluxo
        conditions:
          author_username: govbr-ds
          state: opened
          ruby: |
            !has_fluxo_mr_label? && related_merge_requests.any? && related_merge_requests.any? { |mr| mr.state == 'opened' }
        actions:
          comment_internal: true
          comment: '{{assignees}} Esta issue está relacionada a um MR aberto, mas não tinha a label `fluxo::merge-request`. A tag foi adicionada automaticamente. Se não for o correto, remova.'
          labels:
            - fluxo::merge-request
merge_requests:
  rules:
    - name: MR sem atualização a mais de 3 dias
      conditions:
        date:
          attribute: updated_at
          condition: older_than
          interval_type: days
          interval: 3
        state: opened
        draft: false
        ruby: |
          !has_pending_label? || !has_pending_label? || !has_impacted_label?
      actions:
        comment_internal: true
        comment: |
          {{reviewers}} Este MR está sem atualização há mais de 3 dias. Lembre-se de atualizar os dados (descrição, comentários, labels, etc.) sempre que possível.
