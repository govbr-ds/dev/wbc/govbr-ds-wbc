#!/bin/bash

set -e

cd dist/angular
npm pack
cd ../react
npm pack
cd ../vue
npm pack
cd ../webcomponents
npm pack
