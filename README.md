# Web Components Monorepo

Este monorepo contém a biblioteca de Web Components do [GovBR-DS](https://gov.br/ds) e projetos relacionados.

Saiba mais sobre o NX monorepo em <https://nx.dev/getting-started/intro>.

## Demonstração 🚀

Veja nossos componentes em funcionamento:

- [Versão 1.x](https://gov.br/ds/webcomponents/)
- [Versão 2.x (em desenvolvimento)](https://govbr-ds.gitlab.io/bibliotecas/wbc/govbr-ds-wbc/)

## Uso 📚

Os componentes estão disponíveis no registro [NPMJS](https://www.npmjs.com). Você pode usar o pacote `@govbr-ds/webcomponents` ou um dos wrappers específicos para framework (`@govbr-ds/webcomponents-angular`, `@govbr-ds/webcomponents-react` e `@govbr-ds/webcomponents-vue`), dependendo da tecnologia do seu projeto. Consulte as instruções de uso para cada pacote:

- [Como usar o pacote `@govbr-ds/webcomponents`](packages/webcomponents/README.md)
- [Como usar o pacote `@govbr-ds/webcomponents-angular`](packages/angular/README.md)
- [Como usar o pacote `@govbr-ds/webcomponents-react`](packages/react/README.md)
- [Como usar o pacote `@govbr-ds/webcomponents-vue`](packages/vue/README.md)

## PNPM 📦

Usamos o PNPM neste projeto porque ele oferece uma maneira mais eficiente e rápida de gerenciar dependências em comparação com outros gerenciadores de pacotes. O PNPM utiliza uma abordagem de armazenamento global de pacotes, o que reduz a duplicação de dependências e economiza espaço em disco. Além disso, o PNPM é conhecido por sua velocidade e confiabilidade, tornando o processo de instalação de pacotes mais rápido e menos propenso a erros.

Para instalar o PNPM, siga as instruções no site oficial: [Instalação do PNPM](https://pnpm.io/installation).

## Executando o Projeto 🛠️

Para desenvolver ou estender os componentes da biblioteca `govbr-ds/webcomponents`, siga os passos abaixo:

```bash
git clone https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc
cd govbr-ds-wbc
git checkout <BRANCH-DESEJADA>
pnpm install
```

### Instalação ⚙️

É recomendado que o `nx` seja instalado como dependência global para facilitar o uso dos comandos. Para instalar o `nx`, execute:

```bash
pnpm install -g nx
```

Caso prefira, você pode usar o `pnpm dlx` para executar os comandos sem instalar o `nx` globalmente:

```bash
pnpm dlx nx <comando>
```

Execute os comandos abaixo para instalar as dependências e realizar o build inicial:

```bash
pnpm install
# Execute o build antes de rodar o projeto pela primeira vez
nx run-many -t=build
```

Agora você está pronto para começar a codificar! 😃

### Executar Tarefas 🔧

Para executar tarefas com Nx, use o comando:

```bash
nx <tarefa> <nome-projeto>

ou

nx run-many -t=<tarefa>
```

Exemplo:

```bash
nx build projeto1

ou

nx run-many -t=build
```

#### Tarefas Comuns

As tarefas mais comuns são:

- `build`: Constrói o projeto
- `start`: Inicia o servidor de desenvolvimento
- `eslint`: Executa o eslint
- `stylelint`: Executa o linter de estilos
- `markdownlint`: Executa o linter de markdown

As tarefas são [inferidas automaticamente](https://nx.dev/concepts/inferred-tasks?utm_source=nx_project&utm_medium=readme&utm_campaign=nx_projects) ou definidas nos arquivos `project.json` ou `package.json`.

[Mais sobre a execução de tarefas na documentação &raquo;](https://nx.dev/features/run-tasks?utm_source=nx_project&utm_medium=readme&utm_campaign=nx_projects)

### Construir 📦

Para construir todos os projetos em modo de produção, execute:

```bash
nx run-many -t build
```

### Testes 🧪

As bibliotecas `@govbr-ds/webcomponents` usam [Jest](https://jestjs.io/) para testes unitários e [Puppeteer](https://pptr.dev/) para testes de end to end.

Para rodar todos os testes, execute:

```bash
nx run test
```

### Gerar Componente ️

Para facilitar a criação de novos componentes, utilizamos o [PlopJS](https://plopjs.com/). Para usar o gerador, execute o seguinte comando e siga as instruções no prompt do CLI:

```bash
pnpm run plop
```

Os arquivos gerados podem precisar ser ajustados conforme a necessidade. Embora nem todos os arquivos possam ser criados automaticamente, essa ferramenta facilita a criação de componentes básicos.

## Fluxo de Trabalho 🚨

1. **Criar Componente**: Adicione uma nova pasta para o componente no diretório `packages/webcomponents/src/components/` e crie os arquivos necessários.
1. **Escrever Lógica**: Crie a lógica do componente dentro da pasta.
   1. Implemente a lógica do componente no arquivo `.tsx`.
   1. Importe e sobrescreva o CSS necessário no arquivo `scss`.
   1. Adicione informações relevantes no arquivo `readme.md`.
   1. Coloque os exemplos de uso do componente dos arquivos da pasta `pages`.
   1. Inclua o componente e seus exemplos no array de componentes e exemplos dentro do arquivo `packages/webcomponents/src/index.html`.
1. **Testar Componentes**: Escreva e execute testes unitários e E2E para garantir a qualidade do componente.

É possível usar o `plop`para criar vários arquivos automaticamente para poupar tempo. Consulte a documentação do monorepo para mais detalhes.

### Componentes

Observe que os componentes **devem** seguir um padrão de escrita. Além das nomenclaturas (descritas na Wiki), devemos seguir os padrões de comentários/descrições, de validações e escrita geral dos componentes.

Por exemplo, sempre use os mesmos comentários para os métodos do ciclo de vida dos componentes uma vez que eles são os mesmos para todos os componentes.

### Exemplos

Os exemplos também tem modelos de escrita para ficarem iguais. Observe como os exemplos anteriores foram escritos e usem como base para os novos.

## Regras de Versionamento e Releases 📝

Este projeto segue um padrão para branches e commits. Consulte a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender mais sobre esses padrões.

Utilizamos o **Semantic Release** para gerar versões automaticamente, baseando-nos nos tipos, escopos e mensagens dos commits para determinar o tipo de versão que será publicada (major, minor, ou patch). No entanto, em alguns casos, é necessário que certos commits sejam ignorados na avaliação para o lançamento de uma nova versão. Para esses casos, utilize os tipos e/ou escopos listados abaixo.

### Tipos de Commits que Não Geram Releases

Os seguintes tipos de commit são ignorados na criação de novas versões, pois representam alterações internas ou trabalhos em progresso que não afetam diretamente o comportamento externo do projeto:

- **`build`**: Atualizações no sistema de build ou nas dependências, sem impacto no código final.
- **`chore`**: Tarefas gerais de manutenção que não impactam o produto final ou sua funcionalidade.
- **`ci`**: Ajustes no pipeline de integração contínua (CI), sem alteração direta no comportamento do produto.
- **`test`**: Criação ou modificação de testes, sem alterações nas funcionalidades existentes.
- **`wip`**: Trabalhos em progresso, indicados como incompletos ou ainda não prontos para produção.

### Escopos de Commits que Não Geram Releases

Em alguns casos, o escopo das alterações deixa claro que o commit não deve gerar uma nova versão, mesmo que o tipo de commit seja relevante. Para isso, os escopos abaixo são configurados para ignorar essas alterações no processo de versionamento:

- **`site`**: Modificações no site de documentação, que não alteram o código do produto.
- **`monorepo`**: Alterações feitas especificamente no repositório monorepo, sem impacto direto no projeto ou na biblioteca de componentes.
- **`no-release`**: Qualquer commit marcado explicitamente para não gerar uma nova versão.

## Documentações Complementares 📚

Consulte a seção sobre Web Componente na nossa [Wiki](https://gov.br/ds/wiki/desenvolvimento/web-components) para obter mais informações sobre esse projeto.

Para saber mais detalhes sobre a especificação Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Contribuindo 🤝

Antes de abrir um Merge Request, por favor, leve em consideração as seguintes informações:

- Este é um projeto open-source e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, use um título claro e explicativo para o MR, e siga os padrões estabelecidos em nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir? Consulte o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

## Reportar Bugs/Problemas ou Sugestões 🐛

Para reportar problemas ou sugerir melhorias, abra uma [issue](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/issues/new). Utilize o modelo adequado e forneça o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Precisa de ajuda? 🆘

Por favor, **não** crie issues para perguntas gerais.

Use os canais abaixo para tirar suas dúvidas:

- Site do GovBR-DS [http://gov.br/ds](http://gov.br/ds)
- Web Components [https://gov.br/ds/webcomponents](https://gov.br/ds/webcomponents)
- Canal no Discord [https://discord.gg/U5GwPfqhUP](https://discord.gg/U5GwPfqhUP)

## Créditos 🎉

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') foram desenvolvidos pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') em colaboração com a comunidade.
