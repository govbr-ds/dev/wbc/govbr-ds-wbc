# Design Review - MR !X - Issue #Y

Por favor, preencha as informações abaixo:

- **Issue Original:** #Y
- **Merge Request:** !X

<!-- Preencha a estimativa de tempo gasto usando o comando: /estimate xh -->

<!-- Escolha o Milestone, iteração e due date, se aplicável -->

/label ~"tipo::revisão" ~"design"
