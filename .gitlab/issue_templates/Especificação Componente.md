<!--
ATENÇÃO: Ao especificar este componente, certifique-se de que ele segue os padrões definidos pelo Padrão Digital do Governo.
É fundamental considerar todas as questões de qualidade, incluindo:

- Testes: Garanta que o componente seja completamente testado, incluindo testes unitários e de ponta a ponta (E2E).
- Acessibilidade: Assegure que o componente seja acessível a todos os usuários, incluindo aqueles com deficiências.
- Usabilidade: O componente deve ser fácil de usar e intuitivo.
- Manutenibilidade: O código deve ser fácil de manter e estender no futuro.

Além disso, siga as melhores práticas de desenvolvimento para garantir a robustez e a eficiência do componente.
-->

# Especificação - [COMPONENTE]

## Visão Geral

## Propriedades

<!-- Liste e descreva as propriedades do componente, incluindo seus tipos e valores padrão -->

| Propriedade | Obrigatório | Atributo | Descrição | Tipo | Valor padrão |
| ----------- | ----------- | -------- | --------- | ---- | ------------ |
|             |             |          |           |      |              |

## Eventos

<!-- Liste e descreva os eventos emitidos pelo componente, incluindo quando são emitidos -->

| Evento | Descrição | Tipo | Emitido quando |
| ------ | --------- | ---- | -------------- |
|        |           |      |                |

## Métodos

<!-- Liste e descreva os métodos públicos do componente, incluindo seus parâmetros e valores de retorno -->

| Métodos | Descrição | Parâmetros | Retorno |
| ------- | --------- | ---------- | ------- |
|         |           |            |         |

## Parts

<!-- Liste e descreva as partes do componente que podem ser estilizadas usando a pseudo-classe ::part -->

| Part | Descrição |
| ---- | --------- |
|      |           |
|      |           |
|      |           |

## Slots

<!-- Liste e descreva os slots disponíveis no componente, incluindo seus propósitos -->

| Slot | Descrição |
| ---- | --------- |
|      |           |

### Testes de Acessibilidade

<!-- Liste as partes do componente que são mais importantes para testes de acessibilidade -->

| Parte | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

## Testes Unitários

<!-- Descreva as partes mais importantes do componente que devem ser cobertas por testes unitários -->

| Parte | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

## Testes E2E

<!-- Descreva as partes mais importantes do componente que devem ser cobertas por testes E2E -->

| Parte | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

## Dependências

<!-- Liste e descreva quaisquer dependências externas que o componente possui -->

- XYZ
- ABC

## Subcomponentes

<!-- Liste e descreva quaisquer subcomponentes que são usados dentro deste componente -->

Cada subcomponentes tem um título específico para eles contendo propriedades, eventos, métodos, slots, acessibilidade e dependências.

## Issues Relacionadas

<!-- Liste as issues criadas com base nesta especificação -->

| Issue | Descrição |
| ----- | --------- |
|       |           |
|       |           |
|       |           |

/label ~"tipo::análise" ~"categoria::componente" ~"dev"
