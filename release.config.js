import { branches, changelog, releaseNotesGenerator } from '@govbr-ds/release-config'

export default {
  branches: branches,
  plugins: [
    [
      '@semantic-release/commit-analyzer',
      {
        preset: 'conventionalcommits',
        releaseRules: [
          { breaking: true, release: 'major' },
          { type: '*!', release: 'major' },
          { type: 'build', release: false },
          { type: 'chore', release: false },
          { type: 'ci', release: false },
          { type: 'deprecated', release: 'patch' },
          { type: 'feat', release: 'minor' },
          { type: 'fix', release: 'patch' },
          { type: 'perf', release: 'patch' },
          { type: 'refactor', release: 'patch' },
          { type: 'removed', release: 'minor' },
          { type: 'revert', release: 'minor' },
          { type: 'test', release: false },
          { type: 'wip', release: false },
          { scope: 'site', release: false },
          { scope: 'monorepo', release: false },
          { scope: 'no-release', release: false },
          { scope: 'release-patch', release: 'patch' },
          { scope: 'release-minor', release: 'minor' },
        ],
        parserOpts: {
          noteKeywords: ['BREAKING CHANGE', 'BREAKING CHANGES', 'BREAKING'],
        },
      },
    ],
    releaseNotesGenerator,
    changelog,
    [
      '@semantic-release/exec',
      {
        prepareCmd: 'bash ./release/prepare-release.sh ${nextRelease.version}',
        publishCmd: 'bash ./release/publish-release.sh ${nextRelease.channel}',
      },
    ],
    [
      '@semantic-release/git',
      {
        assets: [
          'apps/site/**/*',
          'package.json',
          'packages/angular/**/*',
          'packages/react/**/*',
          'packages/vue/**/*',
          'packages/webcomponents/**/*',
        ],
        message:
          'chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes} \n\nCommit gerado automaticamente durante o processo de release',
      },
    ],
    [
      '@semantic-release/gitlab',
      {
        successComment:
          ':tada: Merge Request(s) ou Issue(s) adicionados corretamente à versão ${nextRelease.version} :tada:\n\n Para mais detalhes:\n\n - [GitLab release](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/releases/${nextRelease.version})\n - [GitLab tag](https://gitlab.com/govbr-ds/bibliotecas/wbc/govbr-ds-wbc/-/tags/${nextRelease.version})',
        failComment:
          'A release a partir da branch ${branch.name} falhou devido aos seguintes erros:\n\n - ${errors.map((err) => err.message).join("\\n")}',
        failTitle: 'A release automática falhou 🚨',
        labels: 'prioridade::p1, crítico',
        assignee: 'matheus.monteiro, natanael.leite, oliveira.rafael, paula.coelho',
      },
    ],
    [
      'semantic-release-discord',
      {
        notifyOnFail: false,
        onSuccessTemplate: {
          content:
            'A versão v$npm_package_version do nosso pacote `govbr-ds/webcomponents` (incluindo seus wrappers) já está disponível no [NPMJS](https://www.npmjs.com/package/$package_name "Acessar página do pacote NPM")! :partying_face:\n\n Para conhecer melhor as novidades:\n\n :book: [Página de releases]($repo_url/-/releases "Releases")\n\n :computer: [Repositório no Gitlab]($repo_url)\n\n Quer contribuir com o Design System? Consulte nossa [wiki](https://gov.br/ds/wiki/ "Wiki GOVBR-DS") para mais detalhes.',
          embeds: [
            {
              color: 16736031,
              description: '$release_notes',
              title: 'RELEASE NOTES',
              url: '$repo_url/-/releases/v$npm_package_version',
            },
          ],
          threadName: '@govbr-ds/webcomponents v$npm_package_version',
          username: 'GovBR-DS',
        },
      },
    ],
  ],
}
